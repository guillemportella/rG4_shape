##### Preambulations

I added passwordless ssh conections between master and slaves simply by
includding the id_pub.rsa in .ssh/ to .ssh/authorized_keys. This is, I have
authorised myself.

### Start mongod from headnode-1

Since mongod was warning me to use numa interleaving, and numactl was not 
available, I compile it from their Github repo. It was straighforward, only
remember to ./autogen.sh before ./configure and you'll be fine. 
Then, 

```bash
./programs/numactl/bin/numactl --interleave=all \\
./programs/mongodb-linux-x86_64-3.6.3/bin/mongod \\
-dbpath /home/portel01/mongodb \\
--fork --logpath /home/portel01/mongo.log --bind_ip_all
```
**TODO** --> add security to the line above...

Then, to test I did 

```bash 
srun --pty -u zsh -i
```
and from the remove interactive shell, the important bit is 

```bash
ssh -L 27017:localhost:27017 clust1-headnode-1  -fN
```

where I chose 27017 just because. I guess I don't need to change this
port number for each script I run. 

I tested the setup by running the `js` client, and it worked. 

```bash
./programs/mongodb-linux-x86_64-3.6.3/bin/mongo localhost:21003

```
