"""import everything by now"""
from rg4shaper.compare_conditions import (
    compute_significant_g4_based,
    read_in_compressed_fo,
)
import rg4shaper.significant_rg4
import rg4shaper.consensus_rg4
import rg4shaper.document_classes
import rg4shaper.push_controls
import rg4shaper.push_reactivities
import rg4shaper.push_sequences
