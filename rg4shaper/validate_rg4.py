"""Drive the analysis of reactivity data."""
import logging
import sys
import time
from typing import (
    Callable,
    Dict,
    List,
    Union,
)

from pymongo import MongoClient

import rg4shaper.document_classes as order
from rg4shaper.document_classes import Range
from rg4shaper.load_config import config

MONGO_HOST_AND_PORT = config.rg4_react['MONGO_PORT_AND_HOST']

logger = logging.getLogger('rg4_logger')


class ExpvsCntrl(object):
    """Contain missing values"""

    def __init__(self, exp_id: object) -> None:
        self.exp_id = exp_id
        self.tr_range = {}

    def __setitem__(self, key: str, value: List[Range]) -> None:
        self.tr_range.setdefault(key, []).append(value)

    def __len__(self):
        return len(self.tr_range)

    def __repr__(self):
        return vars(self).__str__()


class FalsePositive(object):
    """Contain false positives"""

    def __init__(self, exp_id: object) -> object:
        self.exp_id = exp_id
        self.false_positive = {}

    def __setitem__(self, key: str, value: List[str]) -> None:
        self.false_positive.setdefault(key, []).extend(value)

    def __repr__(self):
        return vars(self).__str__()


# *TODO* Maybe do the same as in missing positive, call the
# function once for each compared condition, and return the same
# object type as report_missing_positives
def report_false_positives(*, list_cntrl_col: List[str],
                           col_signif: object,
                           cntrl2check: str,
                           db: object) -> dict:
    """Return an object with false positives for each experiment.

    Input
    -----
    : list_cntrl_col  - list of str representing collection names with controls
    : col_signif - mongodb collection of significantly diff rG4
    : cntrls2check - list of string containing which controls to check
    : db : the database

    Output
    ------
    : class FalsePositive()
    """

    col_cntrls_negative = [col for col in list_cntrl_col
                           if "negative" in col]

    query_neg = {'control_type': 'negative'}

    # recover a set of transcripts from MongoDB that are really not rG4
    neg_transcripts = []
    for col in col_cntrls_negative:
        neg_col_cursor = db[col].find(query_neg)
        if neg_col_cursor.count() == 0:
            logger.warn(
                f'Querying {query_neg} did not return any match')
        else:
            for neg in neg_col_cursor:
                neg_transcripts.extend(neg['transc_id'])
    neg_transcripts = set(neg_transcripts)

    # query for collections
    query_signif = {
        'cell_type': 'Hela',
        'environment': 'vitro',
        'is_consensus': {"$exists": False},
    }
    matched_exp = col_signif.find(query_signif)

    if matched_exp.count() == 0:
        logger.warn(
            f'Warning, querying {query_signif} did not return any match')
        return FalsePositive(None)

    # the conditions comparisons we want to consider
    diff_to_check = "-".join(sorted([cntrl2check, "Li"]))  # lexicographic
    for doc in matched_exp:
        false_positives = FalsePositive(doc['_id'])
        common = neg_transcripts.intersection(
            doc['conditions_diff_ranges'][diff_to_check].keys())
        if len(common) > 0:
            false_positives[diff_to_check] = list(common)
        yield false_positives
    return


def report_FN_TP(*, list_cntrl_col: List[str],
                 col_signif: object,
                 cntrl2check: str,
                 db: object,
                 max_allowed_gap=-20) -> dict:
    """Generate regions in positive controls that were not found.

    Generator returning result for each experiment/collection matched.
    Input
    -----
    : list_cntrl_col  - list of str representing collection names with controls
    : col_signif - mongodb collection of significantly diff rG4
    : cntrls2check - list of string containing which controls to check
    : db : the database

    Output
    ------
    : class ExpvsCntrl()
    """

    # fill up list of dicts containing the positive controls
    col_cntrls_positive = [col for col in list_cntrl_col
                           if "positive" in col and cntrl2check in col]
    query_positive = {'control_type': {'$regex': cntrl2check}}
    list_pos_ranges = []
    for col in col_cntrls_positive:
        pos_ranges = {}
        p_cursor = db[col].find(query_positive)
        for p in p_cursor:
            # TODO --> more than one range, this should appended to a list
            pos_ranges[p['transc_id']] = Range(*p['range'])
        list_pos_ranges.append(pos_ranges)

    query_signif = {
        'cell_type': 'Hela',
        'environment': 'vitro',
        'is_consensus': {"$exists": False},
    }
    matched_exp = col_signif.find(query_signif)
    if matched_exp.count() == 0:
        logger.warn(f'Querying {query_signif} did not return any match')
        return ExpvsCntrl(None)

    diff_to_check = "-".join(sorted([cntrl2check, "Li"]))
    # I will dot it one experiment at a time. i.e one Object id at a time
    for doc in matched_exp:
        sig_tr_ranges = {}
        r = doc['conditions_diff_ranges'][diff_to_check]
        for k, v in r.items():
            sig_tr_ranges.setdefault(k, [Range(*x) for x in v])

        missing = ExpvsCntrl(doc['_id'])
        present = ExpvsCntrl(doc['_id'])

        for dict_pos_cntrl in list_pos_ranges:
            for k, v in dict_pos_cntrl.items():
                if k not in sig_tr_ranges:
                    missing[k] = v
                else:
                    for sig_range in sig_tr_ranges[k]:
                        ov = order.overlap(sig_range, v)
                        if ov < max_allowed_gap:
                            missing[k] = sig_range
                        else:
                            present[k] = sig_range
        yield missing, present


class ValidateG4(object):
    """Class to perform validation of predicted regions based on controls."""
    cntrls2check = ['PDS', 'K']

    def __init__(self, *, dbname=None, col_sig: object = None,
                 ctrl_basename=None, col_fp_name=None, col_tp_name=None,
                 col_fn_name=None):
        self.db_name = dbname
        self.col_sig_name = col_sig
        self.col_ctrl_basename = ctrl_basename
        self.col_fn_name = col_fn_name
        self.col_tp_name = col_tp_name
        self.col_fp_name = col_fp_name
        self.tp = {}
        self.fn = {}
        self.fp = {}
        self._do_fp = False
        self._do_fn_tp = False

    def __enter__(self):
        """Open the connection, populate controls"""
        client = MongoClient(host=[MONGO_HOST_AND_PORT])
        self.db = client[self.db_name]
        self.start_t = time.clock()
        self.col_sig = self.db[self.col_sig_name]
        self.col_ctrl_names = [collection for collection in
                               self.db.collection_names()
                               if self.col_ctrl_basename in collection]
        if len(self.col_ctrl_names) == 0:
            # TODO change to throw exception
            logger.critical('Could not find a control collection with ',
                            end='')
            logger.critical(f'basename {self.col_ctrl_basename} '
                            f'in database {self.db_name}')
            sys.exit()
        return self

    @property
    def do_fp(self):
        return self._do_fp

    @do_fp.setter
    def do_fp(self, value):
        if isinstance(value, bool):
            self._do_fp = value
        else:
            raise TypeError("do_fp should be a bool")

    @property
    def do_fn_tp(self):
        return self._do_fp

    @do_fn_tp.setter
    def do_fn_tp(self, value):
        if isinstance(value, bool):
            self._do_fn_tp = value
        else:
            raise TypeError("do_fn_tp should be a bool")

    def false_positives(self):
        """Generate the false positives."""
        for sig_c in ValidateG4.cntrls2check:
            for _ in report_false_positives(
                    list_cntrl_col=self.col_ctrl_names,
                    col_signif=self.col_sig,
                    cntrl2check=sig_c,
                    db=self.db):
                if len(_.false_positive) > 0:
                    self.fp = {'id_experiment': _.ex_id,
                               'tr_ranges': _.false_positive,
                               'cell_type': 'Hela',
                               'environment': 'vitro',
                               'condition': sig_c,
                               }
            if len(self.fp) != 0:
                yield self.fp

    def false_neg_true_pos(self):
        """Generate the false negatives and the true positives."""
        for fn, tp in report_FN_TP(
                list_cntrl_col=self.col_ctrl_names,
                col_signif=self.col_sig,
                cntrl2check="PDS",
                db=self.db):
            if len(fn) > 0:
                self.fn = {'id_experiment': fn.exp_id,
                           'tr_ranges': fn.tr_range,
                           'ref_condition': 'PDS',
                           'cell_type': 'Hela',
                           'environment': 'vitro'
                           }
            if len(tp) > 0:
                self.tp = {'id_experiment': tp.exp_id,
                           'tr_ranges': tp.tr_range,
                           'cell_type': 'Hela',
                           'environment': 'vitro',
                           'ref_condition': 'PDS',
                           }
            yield self.fn, self.tp

    def __exit__(self, exc_type, exc_val, exc_tb):
        """Push to the database"""
        if self.do_fp:
            for to_dbase in self.false_positives():
                fp_db = self.db[self.col_fp_name]
                fp_db.insert_many(to_dbase)

        if self.do_fn_tp:
            for to_dbase_fn, to_dbase_tp in self.false_neg_true_pos():
                fn_dbase = self.db[self.col_fn_name]
                fn_dbase.insert_one(to_dbase_fn)
                tp_dbase = self.db[self.col_tp_name]
                tp_dbase.insert_one(to_dbase_tp)

        logger.info("Finished execution in {0:.4f} seconds.".format(
            time.clock() - self.start_t))


def validate_rg4(arguments: Dict[str, Union[str, Callable]]) -> int:
    """Check if we detected positive controls, and avoided negative ones."""
    db_name = arguments['db']
    collection_name = arguments['cn']
    cntrl_basename = arguments['cc']

    with ValidateG4(dbname=db_name,
                    col_sig=collection_name,
                    ctrl_basename=cntrl_basename,
                    col_fp_name='false_positives',
                    col_fn_name='false_negatives',
                    col_tp_name='true_positives',
                    ) as vg4:
        vg4.do_fn_tp = True
        vg4.do_fp = True

    return 1
