"""Add raw reactivities to database

Each transcript reactivity will be stored as a document, along
with the file name and transcript name.
The hope is that accessing these will be much quicker than just
reading from the file and finding the right entry for a given transcript.
"""
import logging
import pickle
import tarfile
from pathlib import PurePosixPath

import pymongo
from bson.binary import Binary
from pymongo import MongoClient

import rg4shaper.compare_conditions as ccg4
from rg4shaper.load_config import config

logger = logging.getLogger("rg4_logger")
MONGO_HOST_AND_PORT = config.rg4_react['MONGO_PORT_AND_HOST']


def push_transcripts(*, fname: str, collection, dict_data: dict) -> int:
    """Push 1d np.array to collection, one file at a time.

    Input
    -----
    fname - the name of the file, added to the document in mongodb
    collection - the open collection in mongodb
    dict_data - a dict of np.array, the key is the transcript name

    Output
    ------
    counts - the number of collections inserted
    """
    to_insert = []
    for k, v in dict_data.items():
        pickled_data = Binary(pickle.dumps(v))
        record = {"fname": fname,
                  "transc_id": k,
                  "reactivities": pickled_data,
                  }
        to_insert.append(record)

    return len(collection.insert_many(to_insert).inserted_ids)


def push_reactivities(*, tar_fn: str, collection):
    """Push reactivities to mongodb from files contained in a tar file."""
    with tarfile.open(tar_fn) as tar_fh:
        for tarinfo in tar_fh.getmembers():
            # important!!! check if it is a file, might contain folder name
            if tarinfo.isreg():
                logger.debug(f"Reading {tarinfo.name}")
                clean_fn = PurePosixPath(tarinfo.name).stem.strip(".reac")
                f = tar_fh.extractfile(tarinfo)
                dict_tr = ccg4.read_in_compressed_fo(f)
                how_many = push_transcripts(fname=clean_fn,
                                            collection=collection,
                                            dict_data=dict_tr)
                logger.info(f"Inserted {how_many} records from {clean_fn}")
    logger.info("Indexing reactivities on fname and transc_id")
    collection.create_index([('transc_id', pymongo.DESCENDING),
                             ('fname', pymongo.DESCENDING)])
    logger.info("Done indexing.")


def reactivities_db(args):
    """Push controls to their database."""
    db_name = args['db']
    col_name = args['cn']
    tar_f_react = args['d'].name
    if not col_name:
        col_name = db_name + ".collection"
    b_clean_collection = args['clean_collection']

    client = MongoClient(host=[MONGO_HOST_AND_PORT])
    db = client[db_name]
    collection = db[col_name]

    if b_clean_collection:
        db.drop_collection(col_name)
        logger.info(f"Dropped {col_name}")
    else:
        push_reactivities(
            tar_fn=tar_f_react,
            collection=collection,
        )
        logger.info(f"Done pushing {tar_f_react}")
        return 1
