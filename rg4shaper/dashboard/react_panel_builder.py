"""Refactoring."""

import logging
import pickle
from collections import namedtuple
from math import pi
from typing import NamedTuple, List, Tuple

import attr
import numpy as np
from bokeh.models import ColumnDataSource, Range1d, BoxAnnotation, HoverTool, \
    SingleIntervalTicker, FixedTicker
from bokeh.models.widgets import Select, Div, CheckboxGroup, TableColumn, \
    DataTable, Slider, RadioButtonGroup
from bokeh.plotting import figure

from ..compare_conditions import read_seq_db
from ..document_classes import Range

react_pair = namedtuple('Reactivities',
                        ['first', 'second'])

logger = logging.getLogger('rg4_logger')

INIT_BOX_PADDING = 25
OPT_TO_METRIC = {0: 'gini', 1: 'rmsd'}


def ticker_func(*, seq):
    """Return formatting for the ticks"""
    tick = 0  # placeholder
    if tick + 1 > len(seq) and tick <= 0:
        tick = seq[tick]
    return tick


@attr.s
class VivoComparisonPanel(object):
    """store only"""
    label: str = attr.ib()
    metric = attr.ib()
    score_slider = attr.ib()
    pval_filter = attr.ib()
    validated_checkbox = attr.ib()
    transc_selector = attr.ib()
    range_selector = attr.ib()
    react_selector = attr.ib()
    overview_table = attr.ib()
    score_threshold: float = attr.ib()
    pvalue_threshold: float = attr.ib()
    validated: bool = attr.ib()
    graph = attr.ib()
    graph_metric = attr.ib()
    data_source = attr.ib()
    data_source_bg = attr.ib()
    data_source_m = attr.ib()

    @validated.validator
    def check(self, attribute, value):
        if not isinstance(value, bool):
            raise TypeError(f"validated should be a bool")


def find_documents(*, threshold, col_sig, inducer, validated, metric,
                   pval=0.05):
    query = {'validated': validated,
             'inducer': inducer,
             'metric': metric,
             '$expr': {'$gt': [{'divide': ['$score', '$records_inducer']},
                               threshold],
                       },
             'pvalues': {'$elemMatch': {'$lte': pval}},
             }
    cursor = col_sig.find(query)
    return cursor


def html_table(*, transcript_n, rg4_n):
    """Return a string with html for the overview_table"""
    html = f"""<style>
    table, th, td {{
        margin-top: 1.5em; 
        margin-bottom: 1.5em; 
        background-color: #F8F8F8;
    }}
    th, td {{
        padding: 5px;
    }}
    th {{
        text-align: left;
    }}
    </style>
    <div style="margin-top: 2.0em; margin-bottom: 2.4em">
    <table style="width:100%;   border: 1px solid #dedede; border-radius:3px;">
        <tr>
        <td></td>
        </tr>
      <tr>
        <td>Total transcripts:</td>
        <td>{transcript_n}</td>
      </tr>
      <tr>
        <td>Total rG4s:</td>
        <td>{rg4_n}</td>
      </tr>
        <tr>
        <td></td>
        </tr>
    </table>
   <div>
    """
    return html


class PanelComparisonVivo(object):
    """Select comparisons vivo data"""

    def __init__(self, *, col_seqs, col_sig, col_react, inducer: str,
                 transc: List, score_threshold: float, pval_threshold: float,
                 validated: bool, metric: str):
        self.col_seqs = col_seqs
        self.col_sig = col_sig
        self.col_react = col_react
        self.transcripts = transc
        self.inducer = inducer
        self.score_threshold = score_threshold
        self.validated = validated
        self.metric = metric
        self.pvalue_threshold = pval_threshold
        self.panels = None
        self.layout = None
        self.data_source = ColumnDataSource(data={'x': np.zeros(1),
                                                  'y1': np.zeros(1),
                                                  'y2': np.zeros(1), })
        self.data_source_metric_vline = ColumnDataSource(data={'x': [0],
                                                               'y': [0]})
        self.data_source_bg_metric = ColumnDataSource(data={'x': [0],
                                                            'y': [0]})

        hover = HoverTool(
            tooltips=[
                ("Base (0-based)", "$index"),
                ("Norm. reactivity", "$y"),
            ],
            mode='mouse')

        self.tools = ['pan', 'xwheel_zoom', 'xbox_zoom', hover, ]

    def __enter__(self):
        self.panels = self.comparison_panel()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        logger.debug("Done!")

    def _make_score_filter(self):
        """Slider widget"""
        w = \
            Slider(start=0.1, end=1, value=0.1, step=.1,
                   title="Replicate fraction (0 to 1)")
        return w

    def _make_metric_button(self):
        """Radio check button for metric"""
        w = \
            RadioButtonGroup(labels=["Gini", "RMSD"], active=0)
        return w

    def _make_pval_filter(self):
        """Slider widget"""
        w = \
            Slider(start=0.0001, end=self.pvalue_threshold,
                   value=0.05, step=.0005,
                   title="p-value", format='0[.]000')
        # w = TextInput(value=str(self.pvalue_threshold),
        #               title="p-value filter (max 0.05)")
        return w

    def _make_validated_box(self):
        w = CheckboxGroup(labels=["Validated"], active=[0])
        return w

    def _make_overview_datatable(self):
        """Table hosting some statistics"""
        data = dict(
            dates=[str(i) for i in range(2)],
            downloads=[str(i) for i in range(2)],
        )
        source = ColumnDataSource(data)

        columns = [
            TableColumn(field="dates", title=""),
            TableColumn(field="downloads", title="Number"),
        ]
        data_table = DataTable(source=source, columns=columns, width=300,
                               height=80, fit_columns=True, row_headers=False)

        return data_table

    def _make_overview_table(self, *, transcript_n, rg4_n):
        """Make a table using html"""
        html = html_table(transcript_n=transcript_n, rg4_n=rg4_n)
        div = Div(text=html)
        return div

    def _make_selector_transcript(self):
        """Populate the transc_id based on the score threshold """
        list_of_transc = sorted(list(set(self.transcripts)))
        w = Select(title=f"Transcripts: {len(list_of_transc)}",
                   value=list_of_transc[0],
                   options=list_of_transc)

        return w

    def _make_selector_ranges_and_pairs(self):
        """Populate the list of ranges for each transcript"""
        query = {'transc_id': sorted(self.transcripts)[0],
                 'validated': True,  # we start with true
                 'metric': self.metric,
                 'inducer': self.inducer,
                 }
        match = self.col_sig.find(query)

        ranges = []
        for m in match:
            r = Range(*m['range'])
            ranges.append(str(r.start) + ":" + str(r.end))

        w_ranges = Select(title=f"Ranges: {len(ranges)}",
                          value=ranges[0],
                          options=ranges)

        query = {'transc_id': sorted(self.transcripts)[0],
                 'validated': True,
                 'metric': self.metric,
                 'range': [int(x) for x in ranges[0].split(":")],
                 'inducer': self.inducer
                 }
        match = self.col_sig.find_one(query)

        the_options = [str(x + 1) for x in range(len(match['react_pairs']))]

        w_pairs = Select(title=f"Replicates: {len(the_options)}",
                         value="1",
                         options=the_options)

        return w_ranges, w_pairs

    def _make_panel_react(self):
        """Create a panel with the reactivities."""

        logger.debug("Inside make panel")
        init_transc = sorted(self.transcripts)[0]
        sequence = read_seq_db(self.col_seqs, init_transc)
        query = {'transc_id': sorted(self.transcripts)[0],
                 'validated': True,
                 'metric': self.metric,
                 'inducer': self.inducer
                 }
        match = self.col_sig.find_one(query)
        __range = Range(*match['range'])
        dbase_id = match["_id"]
        react_1, react_2 = self._get_reactivities_vivo(transc_name=init_transc,
                                                       replicate_index=0,
                                                       dbase_id=dbase_id,
                                                       )

        logger.debug(f"I have the reactivities")

        p = figure(tools=self.tools,
                   x_range=Range1d(start=__range.start - INIT_BOX_PADDING,
                                   end=__range.end + INIT_BOX_PADDING,
                                   bounds=(-5, len(react_1) + 5)),
                   )

        self.data_source.data = {'x': np.arange(len(react_1)), 'y1': react_1,
                                 'y2': react_2}
        p.line('x', 'y1',
               source=self.data_source,
               color="firebrick",
               legend=self.inducer,
               line_width=2)
        p.line('x', 'y2',
               source=self.data_source,
               color="navy",
               legend="Phys.",
               line_width=2)

        p.renderers.extend([BoxAnnotation(left=__range.start,
                                          right=__range.end,
                                          fill_alpha=0.4,
                                          name="range_rg4")])

        p.plot_height = 250
        p.plot_width = 800
        for_labels = {ii: n for ii, n in enumerate(sequence)}
        pre_labels = {ii: '' for ii in range(-INIT_BOX_PADDING, 0)}
        post_labels = {ii: '' for ii in
                       range(len(sequence), len(sequence) + INIT_BOX_PADDING)}
        p.xaxis.major_label_overrides = {**pre_labels, **for_labels,
                                         **post_labels}
        p.xaxis.major_label_overrides = for_labels
        p.xaxis.major_label_orientation = pi / 4
        p.xaxis.ticker = SingleIntervalTicker(interval=1,
                                              desired_num_ticks=1,
                                              num_minor_ticks=0)
        p.yaxis.axis_label = 'Norm. reactivities'
        p.grid.grid_line_alpha = 0.6
        logger.debug("Returning from make panel")
        return p

    def _make_panel_metric(self):
        """Graph the bg_metric and the metric value for a given range"""
        logger.debug("Inside make panel")
        query = {'transc_id': sorted(self.transcripts)[0],
                 'validated': True,
                 'metric': self.metric,
                 'environment': 'vivo',
                 'inducer': self.inducer,
                 }
        match = self.col_sig.find_one(query)

        rep_index = 0
        metric_val = match['metric_val'][rep_index]
        bg_metric = np.array(match['bg_metrics'][rep_index])
        # TODO --> compute a histogram of data...
        hist, edges = np.histogram(bg_metric, density=False, bins=50)
        self.data_source_metric_vline.data['x'] = [metric_val]
        logger.debug(f"Metric for hist is {metric_val}")
        self.data_source_bg_metric.data = {'x': edges[:-1],
                                           'y': hist}
        p = figure(tools=self.tools)
        p.plot_height = 250
        p.plot_width = 800
        p.line(x='x', y='y', source=self.data_source_bg_metric)
        p.ray('x', 'y', source=self.data_source_metric_vline, length=0,
              angle=np.pi / 2, line_width=5)
        p.yaxis.axis_label = 'Prob. background metric'
        p.xaxis.axis_label = 'Metric value'
        return p

    # I have placed the execution of this function in __enter__ not
    # to forget to run it
    def comparison_panel(self):
        """Return score filter, validate check-box and reactivity panel."""
        slider = self._make_score_filter()
        pval_filter = self._make_pval_filter()
        check_box = self._make_validated_box()
        metric_button = self._make_metric_button()
        transc_selector = self._make_selector_transcript()
        range_selector, react_selector = \
            self._make_selector_ranges_and_pairs()
        transc_n, rg4_n = self.__count_transc_and_rg4s()
        overview_table = self._make_overview_table(transcript_n=transc_n,
                                                   rg4_n=rg4_n)
        logger.debug(f"About to make reactivities")
        reactivities_panel = self._make_panel_react()
        metrics_panel = self._make_panel_metric()
        label = Div(
            text=f"""
            <div> 
            <style> hr 
            {{ display: block; 
            margin-top: 0.5em; 
            margin-bottom: 0.5em; 
            margin-left: auto; 
            margin-right: auto; 
            border-style: inset; 
            border-width: 1px; }} 
            </style> 
            </div> 
            <hr> 
            Compared conditions {self.inducer}""")

        # auto store
        self.layout = \
            VivoComparisonPanel(label=label,
                                metric=metric_button,
                                score_slider=slider,
                                pval_filter=pval_filter,
                                validated_checkbox=check_box,
                                transc_selector=transc_selector,
                                range_selector=range_selector,
                                react_selector=react_selector,
                                overview_table=overview_table,
                                score_threshold=self.score_threshold,
                                pvalue_threshold=self.pvalue_threshold,
                                validated=self.validated,
                                graph=reactivities_panel,
                                graph_metric=metrics_panel,
                                data_source=self.data_source,
                                data_source_bg=self.data_source_bg_metric,
                                data_source_m=self.data_source_metric_vline)

        return self.layout

    # Callbacks to update lists and plots

    def _update_replicate_list(self, *, transc, ranges):
        """Update replicates based on the transcript value"""
        logger.debug("In update_replicate_list")
        query = {
            'transc_id': transc,
            'validated': self.layout.validated,
            'range': [*ranges],
            'metric': self.metric,
            'inducer': self.inducer,
            'pvalues': {'$elemMatch': {'$lte': self.layout.pvalue_threshold}},
        }
        # how many of these actually have a pval <= cutoff?

        match = self.col_sig.find_one(query)
        replicates = []
        for i, p in enumerate(match['pvalues']):
            if p <= self.layout.pvalue_threshold:
                replicates.append(str(i + 1))

        self.layout.react_selector.options = replicates
        self.layout.react_selector.title = f"Replicates: {len(replicates)}"

        self.layout.react_selector.value = \
            self.layout.react_selector.options[0]
        logger.debug("Done annotating the list of replicates")

    def __count_transc_and_rg4s(self) -> Tuple[int, int]:
        """Number of transcript and total rg4s for given condition"""
        query = {
            'validated': self.validated,
            'inducer': self.inducer,
            'metric': self.metric,
            '$expr': {'$gt': [{'$divide': ['$score', '$records_inducer']},
                              self.score_threshold],
                      },
            'pvalues': {'$elemMatch': {'$lte': self.pvalue_threshold}},
        }
        match = self.col_sig.find(query)
        total_rg4s = 0
        transc = []
        for m in match:
            counter = 0
            for p in m['pvalues']:
                if p <= self.pvalue_threshold:
                    counter += 1.0
            if counter / m['records_inducer'] >= self.score_threshold:
                total_rg4s += 1
                transc.append(m['transc_id'])

        return len(set(transc)), total_rg4s

    def _update_ranges_list(self, *, transc):
        """Update the list of ranges based on the transcript"""
        logger.debug("In update_range_list")
        query = {
            'transc_id': transc,
            'validated': self.layout.validated,
            'inducer': self.inducer,
            'metric': self.metric,
            '$expr': {'$gt': [{'divide': ['$score', '$records_inducer']},
                              self.layout.score_threshold],
                      },
            'pvalues': {'$elemMatch': {'$lte': self.layout.pvalue_threshold}},
        }
        match = self.col_sig.find(query)

        # actually the p-value changed the score_threshold result
        # i can't be bothered to do it in mongo, i will do it here
        ranges = []
        for m in match:
            counter = 0
            for p in m['pvalues']:
                if p <= self.layout.pvalue_threshold:
                    counter += 1.0
            if counter / m['records_inducer'] >= self.layout.score_threshold:
                r = Range(*m['range'])
                ranges.append(str(r.start) + ":" + str(r.end))
        self.layout.range_selector.options = ranges
        self.layout.range_selector.value = ranges[0]
        self.layout.range_selector.title = f"Ranges: {len(ranges)}"
        return ranges[0]

    def _update_transcript_list(self):
        """Update the list of transcripts given a threshold and validate"""
        logger.debug("In update_transcript_list")
        cursor = find_documents(threshold=self.layout.score_threshold,
                                pval=self.layout.pvalue_threshold,
                                col_sig=self.col_sig,
                                inducer=self.inducer,
                                metric=self.metric,
                                validated=self.layout.validated
                                )
        # actually the p-value changed the score_threshold result
        # i can't be bothered to do it in mongo, i will do it here
        list_transc = []
        for m in cursor:
            counter = 0.0
            for p in m['pvalues']:
                if p <= self.layout.pvalue_threshold:
                    counter += 1.0
            if counter / m['records_inducer'] >= self.layout.score_threshold:
                list_transc.append(m['transc_id'])

        # list_transc = sorted(list(set([c['transc_id'] for c in cursor])))
        sorted_list = sorted(list(set(list_transc)))
        self.layout.transc_selector.title = f"Transcripts: {len(sorted_list)}"
        self.layout.transc_selector.options = sorted_list
        self.layout.transc_selector.value = sorted_list[0]
        return sorted_list[0]

    def _get_reactivities_vivo(self, *, dbase_id,
                               transc_name: str,
                               replicate_index: int) -> NamedTuple:
        """Return np.array of reactivities."""
        logger.debug("In get_reactivities_vivo")
        matches = self.col_sig.find_one({"_id": dbase_id})
        fn_1 = matches['react_pairs'][replicate_index][0]
        fn_2 = matches['react_pairs'][replicate_index][1]
        query_react_1 = {'fname': fn_1, 'transc_id': transc_name}
        query_react_2 = {'fname': fn_2, 'transc_id': transc_name}
        match_1 = self.col_react.find_one(query_react_1)
        match_2 = self.col_react.find_one(query_react_2)
        react_1 = pickle.loads(match_1['reactivities'])
        react_2 = pickle.loads(match_2['reactivities'])

        return react_pair(react_1, react_2)

    def update_graph(self, attr, old, new):
        """Update the graph if the user updates the settings.

        This will be triggered directly if the replicate changes,
        if you change the rest of selectors they will be responsible
        to trigger this function.
        """
        logger.debug(f"In update graph")
        transc = self.layout.transc_selector.value
        __ranges = Range(
            *[int(x) for x in self.layout.range_selector.value.split(":")])
        query = {'transc_id': transc,
                 'validated': self.layout.validated,
                 'metric': self.metric,
                 'range': [*__ranges],
                 'inducer': self.inducer
                 }

        match = self.col_sig.find_one(query)
        dbase_id = match['_id']
        __rep_index = int(new) - 1  # 1-based for display
        metric_val = match['metric_val'][__rep_index]
        bg_metric = np.array(match['bg_metrics'][__rep_index])
        react_1, react_2 = \
            self._get_reactivities_vivo(dbase_id=dbase_id,
                                        transc_name=transc,
                                        replicate_index=__rep_index,
                                        )
        # Here we will only have one box per plot, since the
        # analysis is done per range. This means we don't have to worry
        # about keeping track of a changing number of boxes as we do
        # for the in vitro case
        my_box_annotations = []
        for x in self.layout.graph.renderers:
            if isinstance(x, BoxAnnotation) and x.name == "range_rg4":
                x.visible = False
                my_box_annotations.append(x)
        self.layout.data_source.data = dict(x=np.arange(len(react_1)),
                                            y1=react_1,
                                            y2=react_2)
        # We might not need to update the sequence, or the boxes
        # unless the transcript or ranges change
        sequence = read_seq_db(self.col_seqs, transc)
        for_labels = {ii: n for ii, n in enumerate(sequence)}
        pre_labels = {ii: '' for ii in range(-INIT_BOX_PADDING, 0)}
        post_labels = {ii: '' for ii in
                       range(len(sequence), len(sequence) + INIT_BOX_PADDING)}
        self.layout.graph.xaxis.major_label_overrides = {**pre_labels,
                                                         **for_labels,
                                                         **post_labels}
        self.layout.graph.x_range.start = __ranges.start - INIT_BOX_PADDING
        self.layout.graph.x_range.end = __ranges.end + INIT_BOX_PADDING
        self.layout.graph.x_range.bounds = (-5, len(react_1) + 5)
        self.layout.graph.xaxis.ticker = \
            SingleIntervalTicker(interval=1,
                                 desired_num_ticks=1,
                                 num_minor_ticks=0)
        # Finally update the annotations
        my_box_annotations[0].left = __ranges.start
        my_box_annotations[0].right = __ranges.end
        my_box_annotations[0].fill_alpha = 0.4
        my_box_annotations[0].visible = True

        # update the metric graph
        hist, edges = np.histogram(bg_metric, density=True, bins=50)
        self.data_source_metric_vline.data['x'] = [metric_val]
        self.data_source_bg_metric.data = {'x': edges[:-1],
                                           'y': hist}

        logger.debug("Done updating the plot.")

    def update_on_transc(self, _, __, new):
        """Update the list of ranges and replicates based on transcript"""

        # start by updating the range selector
        first_range = self._update_ranges_list(transc=new)

        # use the first range to update the replicate list
        __ranges = Range(*[int(x) for x in first_range.split(":")])

        logger.debug("About to change the replicate_list")
        self._update_replicate_list(
            transc=new,
            ranges=__ranges,
        )
        logger.debug("The nex think you should see is update_graph()")
        self.update_graph(_, __, "1")

    def update_on_ranges(self, _, __, new):
        """Update the list of replicates if the list on ranges changes"""
        logger.debug(f"ABOUT to change the ranges, score threshold")
        ranges = Range(*[int(x) for x in new.split(":")])
        transc = self.layout.transc_selector.value
        self._update_replicate_list(
            transc=transc,
            ranges=ranges,
        )
        self.update_graph(_, __, "1")

    def __top_level_update(self):
        """Common updates for slider and validated"""

        # the transcript list
        __transc = self._update_transcript_list()
        # the range list
        first_range = self._update_ranges_list(transc=__transc)
        # the replicates
        __ranges = Range(*[int(x) for x in first_range.split(":")])
        self._update_replicate_list(
            transc=self.layout.transc_selector.options[0],
            ranges=__ranges,
        )
        # we can *safely assume* that 1 will be the first replicate
        self.update_graph("", "", "1")

    def update_on_slider(self, _, __, new):
        """Update transcripts based on the slider value"""

        new_score = 0.1
        try:
            new_score = float(new)
        except ValueError:
            logger.error("You did not type a number, did you?")

        if new_score < 0:
            new_score = 0
        if new_score > 1:
            new_score = 1
        logger.debug(f"Type is {type(new)}")
        self.layout.score_threshold = float(new_score)
        self.layout.score_slider.value = float(new_score)
        self.score_threshold = float(new_score)

        transc_n, rg4_n = self.__count_transc_and_rg4s()
        html = html_table(transcript_n=transc_n, rg4_n=rg4_n)
        self.layout.overview_table.text = html
        self.__top_level_update()

    def update_on_metric(self, new):
        """Update everything if the user changes the metric"""
        logger.debug("Changin metric type... wait for it.")
        try:
            self.layout.metric = OPT_TO_METRIC[new]
            self.metric = OPT_TO_METRIC[new]
        except KeyError as e:
            logger.warning(f"Option {new} not in my metric records")
            logger.warning(f"I bet you forgot to add a new option.")
            logger.warning(f"Defaulting to Gini")
            self.layout.metric = 'gini'
            self.layout.metric = 'rmsd'

        transc_n, rg4_n = self.__count_transc_and_rg4s()
        html = html_table(transcript_n=transc_n, rg4_n=rg4_n)
        self.layout.overview_table.text = html
        self.__top_level_update()

    def update_on_validated(self, new):
        """Update transcripts based on the value of validation checkbox"""
        logger.debug("You clicked validated toggle")
        if 0 in new:
            self.layout.validated = True
            self.validated = True
        else:
            self.layout.validated = {'$exists': True}
            self.validated = {'$exists': True}

        transc_n, rg4_n = self.__count_transc_and_rg4s()
        html = html_table(transcript_n=transc_n, rg4_n=rg4_n)
        self.layout.overview_table.text = html
        self.__top_level_update()

    def update_on_pvalue(self, _, __, new):
        """Update everything based on a change in p-value"""
        new_pval = 0.05
        try:
            new_pval = float(new)
        except ValueError:
            logger.error("You did not type a number, did you?")

        # correct input miseries
        if new_pval < 0:
            new_pval = 0.00001
        if new_pval > 0.05:
            new_pval = 0.05

        self.layout.pvalue_threshold = float(new_pval)
        self.layout.pval_filter.value = float(new_pval)
        self.pvalue_threshold = float(new_pval)

        transc_n, rg4_n = self.__count_transc_and_rg4s()
        html = html_table(transcript_n=transc_n, rg4_n=rg4_n)
        self.layout.overview_table.text = html
        self.__top_level_update()

    def update_ticker_start(self, _, __, new):
        """Change the ticker to avoided crowding"""
        if self.layout.graph.x_range.end - new > 90:
            self.layout.graph.xaxis.ticker = FixedTicker()
        else:
            self.layout.graph.xaxis.ticker = \
                SingleIntervalTicker(interval=1,
                                     desired_num_ticks=1,
                                     num_minor_ticks=0)

    def update_ticker_end(self, _, __, new):
        """Change the ticker to avoid crowding"""
        if new - self.layout.graph.x_range.start > 90:
            self.layout.graph.xaxis.ticker = FixedTicker()
        else:
            self.layout.graph.xaxis.ticker = \
                SingleIntervalTicker(interval=1,
                                     desired_num_ticks=1,
                                     num_minor_ticks=0)

##############################################################################
