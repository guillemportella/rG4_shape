"""Compose the bokeh dashboard to show rG4s found in significant_rg4"""
import logging
import time
from functools import partial

from bokeh.layouts import layout
from bokeh.models import WidgetBox
from pymongo import MongoClient

from . import react_panel_builder_all as rpb_a
from ..load_config import config, read_user_config_viewer

MONGO_HOST_AND_PORT = config.rg4_react['MONGO_PORT_AND_HOST']
COL_SEQS_NAME = config.dashboard['COLLECTION_SEQS']
COLLECTION_REACT_FN = config.dashboard['COLLECTION_RAW_REACTIVITIES']
COLLECTION_SIGNIFICANT_FN = config.dashboard['COLLECTION_SIGNIFICANT_FN']
DB_NAME = config.dashboard['MONGODB_DATABASE_NAME']

logger = logging.getLogger('rg4_logger')

logger.debug(f"Name of singif is {COLLECTION_SIGNIFICANT_FN}")


class DashBoardDriver(object):
    """Draw dashboard for experimental dataset exploration"""

    def __init__(self, *, db_name: str, col_signif_name: str,
                 col_react_name: str, col_seqs_name: str):
        self.db_name = db_name
        self.col_seqs_name = col_seqs_name
        self.col_signif_name = col_signif_name
        self.col_react_name = col_react_name
        self.pvalue_threshold = 0.05  # max value
        self.score_threshold = 0.04  # max value

    def __enter__(self):
        """Open the connections"""
        client = MongoClient(host=[MONGO_HOST_AND_PORT])
        db = client[self.db_name]
        self.start_t = time.clock()
        self.col_signif = db[self.col_signif_name]
        self.col_react = db[self.col_react_name]
        self.col_seqs = db[self.col_seqs_name]
        self._setup_conditions()
        return self

    def _setup_conditions(self):
        """Initialize the panels"""
        pass

    def dashboard(self):
        """Place the dashboards, return the number of panels placed.
         Returns

         -------
         layouts - dict of layouts, the keys are the comparison conditions
         """
        new_layout = {}
        logger.debug(f"Going to setup the dashboard")
        with rpb_a.PanelBuilder(col_signif=self.col_signif,
                                col_react=self.col_react,
                                col_seqs=self.col_seqs,
                                pvalue_threshold=self.pvalue_threshold,
                                score_threshold=self.score_threshold,
                                ) as builder:
            layout = builder.panels

            layout.table_source.on_change('selected', builder.on_table)
            layout.pvalue_slider.on_change('value',
                                           builder.update_on_pval_slider)
            layout.score_slider.on_change('value',
                                          builder.update_on_score_slider)

            layout.metric_button.on_click(builder.update_on_metric)

            layout.validated_box.on_click(builder.update_on_validated)

            layout.search_input.on_change('value', builder.search_transc)

            for sp in layout.sub_panels.values():
                sp.env_ionic.on_change('value', builder.update_on_env)

            layout.replicate_sel.on_change('value',
                                           builder.update_on_replicate)
            layout.graph.x_range. \
                on_change("start", builder.update_ticker_start)

        return layout

    def __exit__(self, exc_type, exc_val, exc_tb):
        """A silly little check so far."""
        logger.debug("Finished execution in {0:.2f} seconds.".format(
            time.clock() - self.start_t))


def serve_rg4_significant(*, conf_file: str):
    """Return a reactivities dashboard for in vivo results
    :param conf_file: the configuration file
    """

    def react_significant(doc, *, conf_file):
        """Return the doc after placing the dashboard elements"""
        if conf_file is not None:
            read_user_config_viewer(logger=logger, user_config_fn=conf_file)
        with DashBoardDriver(db_name=DB_NAME,
                             col_signif_name=COLLECTION_SIGNIFICANT_FN,
                             col_seqs_name=COL_SEQS_NAME,
                             col_react_name=COLLECTION_REACT_FN) as db:
            to_layout = []
            v = db.dashboard()
            to_layout.extend([[v.label]])
            to_layout.extend([[v.validated_box, v.metric_button]])
            to_layout.extend([[[[v.score_slider], [v.pvalue_slider],
                                [v.search_input],
                                ], [v.overview_table]]])
            to_layout.extend([[v.table_selector]])
            to_layout.extend([
                [
                    WidgetBox(v.sub_panels['Reference'].label,
                              v.sub_panels['Reference'].env_ionic,
                              ),
                    WidgetBox(v.sub_panels['Inducer'].label,
                              v.sub_panels['Inducer'].env_ionic,
                              ),
                ]
            ])
            to_layout.extend([v.replicate_sel])
            to_layout.extend([[v.graph]])

        document = layout(to_layout, sizing_mode='scale_width', width=800)
        doc.add_root(document)
        logger.debug("Brilliant!")
        doc.title = "GPC_rG4_vivo"

    return partial(react_significant, conf_file=conf_file)
