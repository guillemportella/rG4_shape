"""Build the dashboard for the comparisons across in vivo comparisons."""
import logging
import time
from functools import partial

from bokeh.layouts import layout
from bokeh.models import WidgetBox
from pymongo import MongoClient

from . import explore_panel_builder as epb
from ..load_config import config, read_user_config_viewer

MONGO_HOST_AND_PORT = config.rg4_react['MONGO_PORT_AND_HOST']
COL_SEQS_NAME = config.dashboard['COLLECTION_SEQS']
COLLECTION_REACT_FN = config.dashboard['COLLECTION_RAW_REACTIVITIES']
COLLECTION_DATASET_FN = config.dashboard['COLLECTION_DATASET_FN']
DB_NAME = config.dashboard['MONGODB_DATABASE_NAME']

logger = logging.getLogger('rg4_logger')


class DashBoardDriverExplorer(object):
    """Draw dashboard for experimental dataset exploration"""

    def __init__(self, *, db_name: str, col_dataset_name: str,
                 col_react_name: str, col_seqs_name: str):
        self.col_cntrl_dict = {}
        self.db_name = db_name
        self.col_seqs_name = col_seqs_name
        self.col_dataset_name = col_dataset_name
        self.col_react_name = col_react_name
        self.cntrl_names = ['PDS', 'K']
        # ONLY FOR TESTING
        self.rts_threshold = 0.4
        self.pvalue_threshold = 0.05  # max value

    def __enter__(self):
        """Open the connections"""
        client = MongoClient(host=[MONGO_HOST_AND_PORT])
        db = client[self.db_name]
        self.start_t = time.clock()
        self.col_dataset = db[self.col_dataset_name]
        self.col_react = db[self.col_react_name]
        self.col_seqs = db[self.col_seqs_name]
        pds = db['control.positive_KPDS']
        k = db['control.positive_K']
        self.col_cntrl_dict = {'PDS': pds, 'K': k}
        self._setup_conditions()
        return self

    def _setup_conditions(self):
        """Initialize the panels"""
        pass

    def dashboard(self):
        """Place the dashboards, return the number of panels placed.
         Returns

         -------
         layouts - dict of layouts, the keys are the comparison conditions
         """
        new_layout = {}
        for cntrl_type in self.cntrl_names:
            logger.debug(f"Going to setup the dashboard")
            with epb.PanelExplorer(cntrl_type=cntrl_type,
                                   col_dataset=self.col_dataset,
                                   col_react=self.col_react,
                                   col_seqs=self.col_seqs,
                                   col_cntrl=self.col_cntrl_dict[cntrl_type],
                                   rts_threshold=self.rts_threshold,
                                   pvalue_threshold=self.pvalue_threshold
                                   ) as explorer:
                new_layout[cntrl_type] = explorer.panels

                new_layout[cntrl_type].table_source.on_change('selected',
                                                              explorer.on_table)
                new_layout[cntrl_type].graph.x_range. \
                    on_change("start", explorer.update_ticker_start)

                for sp in new_layout[cntrl_type].sub_panels:
                    sp.environment_sel.on_change("value",
                                                 explorer.update_on_env)
                    sp.ionic_sel.on_change("value",
                                           explorer.update_on_ionic)
                    sp.replicate_sel.on_change("value",
                                               explorer.update_on_replicate)

        return new_layout

    def __exit__(self, exc_type, exc_val, exc_tb):
        """A silly little check so far."""
        logger.debug("Finished execution in {0:.2f} seconds.".format(
            time.clock() - self.start_t))


def serve_react_explorer(*, conf_file):
    """Return a reactivities dashboard for in vivo results
    :param control_type:
    """

    def react_vivo(doc, *, conf_file):
        """Return the doc after placing the dashboard elements"""
        if conf_file is not None:
            read_user_config_viewer(logger=logger, user_config_fn=conf_file)
        with DashBoardDriverExplorer(db_name=DB_NAME,
                                     col_dataset_name=COLLECTION_DATASET_FN,
                                     col_seqs_name=COL_SEQS_NAME,
                                     col_react_name=COLLECTION_REACT_FN) as db:
            to_layout = []
            for v in db.dashboard().values():
                to_layout.extend([[v.label]])
                to_layout.extend([[v.table_selector]])
                to_layout.extend([
                    [
                        WidgetBox(v.sub_panels[0].label,
                                  v.sub_panels[0].environment_sel,
                                  v.sub_panels[0].ionic_sel,
                                  v.sub_panels[0].replicate_sel, width=300),
                        WidgetBox(v.sub_panels[1].label,
                                  v.sub_panels[1].environment_sel,
                                  v.sub_panels[1].ionic_sel,
                                  v.sub_panels[1].replicate_sel, width=300),
                    ]
                ])
                to_layout.extend([[v.graph]])

        document = layout(to_layout, sizing_mode='scale_width', width=800)
        doc.add_root(document)
        logger.debug("Brilliant!")
        doc.title = "GPC_rG4_vivo"

    return partial(react_vivo, conf_file=conf_file)
