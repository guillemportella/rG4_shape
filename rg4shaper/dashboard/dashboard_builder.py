"""Build the dashboard for the comparisons across in vitro comparisons."""
import logging
import time
from functools import partial

from bokeh.layouts import layout
from pymongo import MongoClient

from . import react_panel_builder_vitro as rpb
from ..document_classes import Range
from ..load_config import config, read_user_config_viewer

MONGO_HOST_AND_PORT = config.rg4_react['MONGO_PORT_AND_HOST']
COL_SEQS_NAME = config.dashboard['COLLECTION_SEQS']
DB_NAME = config.dashboard['MONGODB_DATABASE_NAME']
COLLECTION_REACT_FN = config.dashboard['COLLECTION_RAW_REACTIVITIES']
COLLECTION_SIG_FN = config.dashboard[
    'COLLECTION_RANGES_DIFF_CONDITIONS']
COLLECTION_TRUE_POSITIVES = config.dashboard['COLLECTION_TRUE_POSITIVES']
COLLECTION_FALSE_NEGATIVES = config.dashboard['COLLECTION_FALSE_NEGATIVES']

logger = logging.getLogger('rg4_logger')


class DashboardDriver(object):

    def __init__(self, db_name, col_sig: object,
                 col_react: object, col_seqs_name):
        self.col_seqs_name = col_seqs_name
        self.db_name = db_name
        self.col_sig_name = col_sig
        self.col_react_name = col_react
        self.dict_conditions = {}
        self.count_replicates = {}
        self.replicate_to_id = {}
        self.vitro_differences = []

    def __enter__(self):
        """Open the connection"""
        client = MongoClient(host=[MONGO_HOST_AND_PORT])
        db = client[self.db_name]
        self.start_t = time.clock()
        self.col_sig = db[self.col_sig_name]
        self.col_react = db[self.col_react_name]
        self.col_seqs = db[self.col_seqs_name]
        self._setup_conditions()
        return self

    def _setup_conditions(self) -> None:
        query = {
            'cell_type': 'Hela',
            'environment': 'vitro',
            'is_consensus': {'$exists': False},
        }

        matches = self.col_sig.find(query)
        tmp = []
        for i, m in enumerate(matches):
            the_keys = m['conditions_diff_ranges'].keys()
            self.replicate_to_id[i] = m["_id"]
            tmp.extend(list(the_keys))
            for k in the_keys:
                d = {
                    key: [Range(*val) for val in v]
                    for key, v in m['conditions_diff_ranges'][k].items()
                }
                self.dict_conditions.setdefault(k, []).append(d)
                self.count_replicates[k] = \
                    self.count_replicates.setdefault(k, 0) + 1

        self.vitro_differences = list(set(tmp))

    def dashboard(self):
        """Place the dashboards, return the number of panels placed.
        Returns
        -------
        layouts - dict of layouts, the keys are the comparison conditions
        """

        new_layout = {}
        for comp_name in self.vitro_differences:
            # for comp_name in [test_name]:
            with rpb.PanelComparison(repl=self.count_replicates[comp_name],
                                     comp_name=comp_name,
                                     col_sig=self.col_sig,
                                     col_react=self.col_react,
                                     tr_dict=self.dict_conditions[comp_name],
                                     col_seqs=self.col_seqs,
                                     rep2id=self.replicate_to_id,
                                     ) as pcomp:
                new_layout[comp_name] = pcomp.panels

                # let's add the controller
                new_layout[comp_name].replicates. \
                    on_change("value", pcomp.update_graph)

                new_layout[comp_name].graph.x_range. \
                    on_change("start", pcomp.update_ticker_start)

                new_layout[comp_name].transcripts. \
                    on_change('value', pcomp.update_on_transcript)

        return new_layout

    def __exit__(self, exc_type, exc_val, exc_tb):
        """A silly little check so far."""
        if len(self.vitro_differences) == 0:
            logger.warning("There were no differences to be computed")
        else:
            logger.info("Finished execution in {0:.2f} seconds.".format(
                time.clock() - self.start_t))


class DashBoardDriverValidation(DashboardDriver):
    """Class to build Validation dashboards for in vitro data"""

    def __init__(self, db_name=None, col_sig: object = None,
                 col_react: object = None, col_val=None, col_seqs=None):
        super().__init__(db_name=db_name, col_sig=col_sig, col_react=col_react,
                         col_seqs=col_seqs)
        self.col_val_name = col_val

    def __enter__(self):
        """Open the connection"""
        client = MongoClient(host=[MONGO_HOST_AND_PORT])
        db = client[self.db_name]
        self.start_t = time.clock()
        self.col_sig = db[self.col_sig_name]
        self.col_val = db[self.col_val_name]
        self.col_react = db[self.col_react_name]
        self._setup_conditions()
        return self

    def _setup_conditions(self) -> None:
        query = {
            'cell_type': 'Hela',
            'environment': 'vitro'
        }

        matches = self.col_val.find(query)
        self.vitro_differences = ['K-Li', 'K-PDS', 'Li-PDS']
        for i, m in enumerate(matches):
            self.replicate_to_id[i] = m["id_experiment"]
            for k in self.vitro_differences:
                d = {
                    key: [Range(*val) for val in v]
                    for key, v in m['tr_ranges'].items()
                }
                self.dict_conditions.setdefault(k, []).append(d)
                self.count_replicates[k] = \
                    self.count_replicates.setdefault(k, 0) + 1


def serve_react(*, conf_file):
    """Driver for dashboard building.

    Creates an embeddable Bokeh dashboard.
    """

    def react_dashboard(doc, *, conf_file):
        if conf_file is not None:
            read_user_config_viewer(logger=logger, user_config_fn=conf_file)
        with DashboardDriver(db_name=DB_NAME,
                             col_sig=COLLECTION_SIG_FN,
                             col_react=COLLECTION_REACT_FN,
                             col_seqs_name=COL_SEQS_NAME,
                             ) as db:
            to_layout = []
            for v in db.dashboard().values():
                to_layout.extend([[v.label]])
                to_layout.extend([[v.replicates, v.transcripts]])
                to_layout.extend([v.graph])

        document = layout(to_layout, sizing_mode='scale_width')
        doc.add_root(document)
        doc.title = "GPC_rG4"

    return partial(react_dashboard, conf_file=conf_file)


def serve_validations(*, control_type='tp', conf_file):
    """Function returning validation with params depend on control_type"""

    if control_type not in ['tp', 'fn']:
        raise ValueError('control_type must be either tp or fn')
    else:
        if control_type == 'tp':
            col_val = COLLECTION_TRUE_POSITIVES
        elif control_type == "fn":
            col_val = COLLECTION_FALSE_NEGATIVES

    def validation(doc, *, conf_file):
        if conf_file is not None:
            read_user_config_viewer(logger=logger, user_config_fn=conf_file)
        with DashBoardDriverValidation(db_name=DB_NAME,
                                       col_sig=COLLECTION_SIG_FN,
                                       col_react=COLLECTION_REACT_FN,
                                       col_val=col_val,
                                       col_seqs=COL_SEQS_NAME,
                                       ) as db:
            to_layout = []
            for v in db.dashboard().values():
                to_layout.extend([[v.label]])
                to_layout.extend([[v.replicates, v.transcripts]])
                to_layout.extend([v.graph])

        document = layout(to_layout, sizing_mode='scale_width')
        doc.add_root(document)
        doc.title = "GPC_rG4_validation"

    return partial(validation, conf_file=conf_file)
