"""Build the panels to display rG4 ranges and reactivity comparisons"""

import logging
import math
import pickle
from collections import namedtuple
from typing import List, Dict, Tuple

import attr
import numpy as np
from bokeh.models import Slider, Div, Select, ColumnDataSource, HoverTool, \
    TableColumn, DataTable, Range1d, BoxAnnotation, FixedTicker, \
    SingleIntervalTicker, RadioButtonGroup, CheckboxGroup, Selection, TextInput
from bokeh.plotting import figure

from ..compare_conditions import read_seq_db
from ..document_classes import Range

react_pair = namedtuple('Reactivities',
                        ['first', 'second'])

logger = logging.getLogger('rg4_logger')

INIT_BOX_PADDING = 25
OPT_TO_METRIC = {0: 'rmsd', 1: 'integration'}

ENV_OPT = {'vivo': ['K', 'KPDS', 'KcPDS', 'KPhenDC3'],
           'vitro': ['Li', 'K', 'KPDS', 'KcPDS']
           }

conditions = namedtuple("conditions",
                        ['ionic', 'environment'])

REFERENCES_LIST = [conditions('Li', 'vitro'),
                   conditions('K', 'vitro'),
                   conditions('K', 'vivo')]
INDUCERS_LIST = [conditions('K', 'vivo'),
                 conditions('KPDS', 'vivo'),
                 conditions('KcPDS', 'vivo'),
                 conditions('KPhenDC3', 'vivo'), ]

EXP_TYPE = {'Reference': REFERENCES_LIST, 'Inducer': INDUCERS_LIST}


@attr.s
class SubPanel(object):
    label = attr.ib()
    env_ionic = attr.ib()


@attr.s
class ComparisonPanel(object):
    """Store panel info"""
    label: str = attr.ib()
    validated_box = attr.ib()
    metric_button = attr.ib()
    overview_table = attr.ib()
    score_slider = attr.ib()
    pvalue_slider = attr.ib()
    search_input = attr.ib()
    table_selector = attr.ib()
    table_source = attr.ib()
    graph = attr.ib()
    data_source: ColumnDataSource = attr.ib()
    replicate_sel = attr.ib()
    sub_panels: List[SubPanel] = attr.ib(default=attr.Factory(list))


transc_range = namedtuple("Transc_range", ['transc', 'range', 'score'])


def make_html_table(*, transcript_n, rg4_n):
    """Return a string with html for the overview_table"""
    html = f"""<style>
    <style> hr 
    {{ display: block; 
    margin-top: 0.5em; 
    margin-bottom: 0.5em; 
    margin-left: auto; 
    margin-right: auto; 
    border-style: inset; 
    border-width: 1px; }} 
    table, th, td {{
        margin-top: 1.9em; 
        margin-bottom: 1.9em; 
    }}
    th, td {{
        padding: 5px;
    }}
    th {{
        text-align: left;
    }}
    </style>
    <div style="margin-top: 2em; margin-bottom: 2em">
    <hr> 
    <table style="width:100%">
      <tr style="border-bottom: 1px solid #000;">
      </tr>
      <tr>
        <td>Total transcripts:</td>
        <td>{transcript_n}</td>
      </tr>
      <tr>
        <td>Total rG4s:</td>
        <td>{rg4_n}</td>
      </tr>
    </table>
   <hr> 
   <div>
    """
    return html


class PanelBuilder(object):

    def __init__(self, *, col_signif, col_react, col_seqs,
                 pvalue_threshold: float, score_threshold: float):
        """Initialize"""
        self.col_signif = col_signif
        self.col_react = col_react
        self.col_seqs = col_seqs
        self.reference: conditions = REFERENCES_LIST[0]
        self.inducer: conditions = INDUCERS_LIST[0]
        self.metric: str = OPT_TO_METRIC[0]
        self.validated: bool = True
        self.pvalue_threshold = pvalue_threshold
        self.score_threshold = score_threshold
        self.panels = None
        self.layout = None
        self.current_transc = None
        self.sub_panels: Dict[str, SubPanel] = None
        self.replicate_list: List[int] = None
        self.graph = None
        self.replicate_selector = None
        self.data_source = ColumnDataSource(data={'x': np.zeros(1),
                                                  'y1': np.zeros(1),
                                                  'y2': np.zeros(1), },
                                            )
        self.table_source = ColumnDataSource(data={'transcript': [],
                                                   'range': [],
                                                   'score': [],
                                                   })
        self.columns_table = [
            TableColumn(field='transcript', title='Transcript'),
            TableColumn(field='range', title='Range'),
            TableColumn(field='score', title='Score'),
        ]
        hover = HoverTool(
            tooltips=[
                ("Base (0-based)", "$index"),
                ("Norm. reactivity", "$y"),
            ],
            mode='mouse')

        self.tools = ['pan', 'xwheel_zoom', 'xbox_zoom', hover, ]

    def __enter__(self):
        self.panels = self.comparison_panel()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        logger.debug(f"Done building exploratory panel")
        pass

    ##########################################################################
    # start the initial layout of the panels
    ##########################################################################

    def _make_slider(self, *, name: str, start: float, end: float,
                     value: float,
                     step: float, format="0[.]00"):
        """Make a slider"""
        w = \
            Slider(start=start, end=end, value=value, step=step,
                   title=name, format=format)
        return w

    def _make_validated_box(self):
        w = CheckboxGroup(labels=["Validated"], active=[0])
        return w

    def _make_metric_button(self):
        """Radio check button for metric"""
        w = \
            RadioButtonGroup(labels=["RMSD", "Integration"],
                             active=0)
        return w

    def _make_texinput(self):
        w = TextInput(value="", title="Search transcript name")
        return w

    def __filter_transcripts(self) -> List[transc_range]:
        """Filtering the transcripts based on thresholds"""

        query = {
            'reference_ionic': self.reference.ionic,
            'reference_environment': self.reference.environment,
            'inducer_ionic': self.inducer.ionic,
            'inducer_environment': self.inducer.environment,
            'validated': self.validated,
            'metric': self.metric,
            'pvalues': {'$elemMatch': {'$lte': self.pvalue_threshold}},
            '$expr': {'$gte': [{'$divide': ['$score', '$records_inducer']},
                               self.score_threshold],
                      },
        }
        t_r = []
        # the p-values threshold can change the score, re-calculate here
        for m in self.col_signif.find(query):
            counter = 0
            for p in m['pvalues']:
                if p < self.pvalue_threshold:
                    counter += 1
            if counter / m['records_inducer'] >= self.score_threshold:
                t_r.append(transc_range(m['transc_id'],
                                        Range(*m['range']),
                                        m['score'] / m['records_inducer']))

        return sorted(t_r, key=lambda x: x.score, reverse=True)

    def _make_table_selector(self):
        """Create the first version of the transcript table.

        This is the table you get when you load the application. The
        threshold values to filter results are taken from the default.

        """
        list_of_transc = self.__filter_transcripts()
        for l in list_of_transc:
            self.table_source.data['transcript'].append(l.transc)
            self.table_source.data['range'].append(l.range)
            self.table_source.data['score'].append(l.score)
        w = \
            DataTable(source=self.table_source, columns=self.columns_table)
        # pre-select the first entry
        w.source.selected = Selection()
        w.source.selected.indices = [0]

        return w

    def __count_transc_and_rg4s(self) -> Tuple[int, int]:
        """Number of transcript and total rg4s for given condition

        Make sure to call this after the data table is in place or
        up to date, as we rely on it.

        """
        total_rg4s = len(self.table_source.data['transcript'])
        distinct_transc = len(set(self.table_source.data['transcript']))
        return distinct_transc, total_rg4s

    def _make_overview_table(self):
        """Make a table using html"""
        transc_n, rg4_n = self.__count_transc_and_rg4s()
        html = make_html_table(transcript_n=transc_n, rg4_n=rg4_n)
        div = Div(text=html)
        return div

    def _get_reactivities(self, *, transc, fn1, fn2) -> Dict[str, List[float]]:
        """Return the reactivities"""
        react = {}
        for fn in [fn1, fn2]:
            query = {'fname': fn,
                     'transc_id': transc
                     }
            react[fn] = pickle.loads(
                self.col_react.find_one(query)['reactivities'])

        return react

    def _make_subpanels(self) -> Dict[str, SubPanel]:
        """Build two subpanels"""
        sub_panels = {}
        for t_exp, cond in EXP_TYPE.items():
            list_options = [" - ".join([x.ionic, x.environment]) for x in cond]
            env_ionic_sel = Select(title="Conditions",
                                   value=next(iter(list_options)),
                                   options=list_options)

            label = Div(
                text=f"""
            <div> 
            <style> hr 
            {{ display: block; 
            margin-top: 0.5em; 
            margin-bottom: 0.5em; 
            margin-left: auto; 
            margin-right: auto; 
            border-style: inset; 
            border-width: 1px; }} 
            </style> 
            </div> 
            {t_exp} 
            <hr> """)
            sub_panels[t_exp] = SubPanel(label, env_ionic_sel)
        return sub_panels

    def _make_replicate_selector(self, *, table_selector):
        """Populate the replicate selector.

        It will read the transc_id and range selection from the Datatable,
        and the conditions for reference and induced from the selectors.

        We assume that if that particular transc_id and range is selectable
        is because it has been pre-filtered based on the metric, pvalue
        and score, so I won't do any further filtering.

        I do have to re-filter again based on the p-value, perhaps I
        could optimize this later.

        """
        ref_env = self.sub_panels['Reference'].env_ionic.value.split(" - ")[1]
        ref_ionic = self.sub_panels['Reference']. \
            env_ionic.value.split(" - ")[0]
        ind_env = self.sub_panels['Inducer'].env_ionic.value.split(" - ")[1]
        ind_ionic = self.sub_panels['Inducer'].env_ionic.value.split(" - ")[0]
        index = \
            next(iter(table_selector.source.selected.indices))
        list_repl = []
        if len(self.table_source.data['transcript']) > 0:
            transc_id = self.table_source.data['transcript'][index]
            query = {'transc_id': transc_id,
                     'reference_environment': ref_env,
                     'reference_ionic': ref_ionic,
                     'inducer_environment': ind_env,
                     'inducer_ionic': ind_ionic,
                     'metric': self.metric
                     }
            pvalues = self.col_signif.find_one(query)['pvalues']

            list_repl = [str(i + 1) for i, x in enumerate(pvalues)
                         if x < self.pvalue_threshold]

        value_sel = next(iter(list_repl)) if len(list_repl) > 0 else "-"
        replicate_sel = Select(title=f"Replicate: {len(list_repl)}",
                               value=value_sel,
                               options=list_repl)

        return replicate_sel

    def __find_fnames_for_react(self, *, transc_id: str, ranges: Range) -> \
            List[str]:
        """Return the filenames according to the condition subpannels"""

        ref_env = self.sub_panels['Reference'].env_ionic.value.split(" - ")[1]
        ref_ionic = self.sub_panels['Reference']. \
            env_ionic.value.split(" - ")[0]
        ind_env = self.sub_panels['Inducer'].env_ionic.value.split(" - ")[1]
        ind_ionic = self.sub_panels['Inducer'].env_ionic.value.split(" - ")[0]

        query = {'transc_id': transc_id,
                 'reference_environment': ref_env,
                 'reference_ionic': ref_ionic,
                 'range': {'$eq': ranges},
                 'inducer_environment': ind_env,
                 'inducer_ionic': ind_ionic,
                 'validated': self.validated,
                 'metric': self.metric
                 }

        match = self.col_signif.find_one(query)
        replicate_n = int(self.replicate_selector.value) - 1
        fnames = match['react_pairs'][replicate_n]

        return fnames

    def _make_panel_react(self):
        """Build the graph

        Initially use the first element in the table to show the data

        """
        if len(self.table_source.data['transcript']) > 0:
            transc = next(iter(self.table_source.data['transcript']))
            ranges = next(iter(self.table_source.data['range']))
            sequence = read_seq_db(self.col_seqs, transc)
            fn1, fn2, *_ = self.__find_fnames_for_react(transc_id=transc,
                                                        ranges=ranges)
            d_react = self._get_reactivities(transc=transc,
                                             fn1=fn1,
                                             fn2=fn2,
                                             )

            p = figure(tools=self.tools,
                       x_range=Range1d(start=ranges.start - INIT_BOX_PADDING,
                                       end=ranges.end + INIT_BOX_PADDING,
                                       bounds=(-5, len(d_react[fn1]) + 5),
                                       ))
            self.data_source.data = {'x': np.arange(len(d_react[fn1])),
                                     'y1': d_react[fn1],
                                     'y2': d_react[fn2]}
            p.line('x', 'y1',
                   source=self.data_source,
                   color="firebrick",
                   legend='Reference',
                   line_width=2)
            p.line('x', 'y2',
                   source=self.data_source,
                   color="navy",
                   legend='Inducer',
                   line_width=2)

            p.renderers.extend([BoxAnnotation(left=ranges.start,
                                              right=ranges.end,
                                              fill_alpha=0.4,
                                              name="range_rg4")])
            p.plot_height = 250
            p.plot_width = 800
            for_labels = {ii: n for ii, n in enumerate(sequence)}
            pre_labels = {ii: '' for ii in range(-INIT_BOX_PADDING, 0)}
            post_labels = {ii: '' for ii in
                           range(len(sequence),
                                 len(sequence) + INIT_BOX_PADDING)}
            p.xaxis.major_label_overrides = {**pre_labels, **for_labels,
                                             **post_labels}
            p.xaxis.major_label_orientation = math.pi / 4
            p.xaxis.ticker = SingleIntervalTicker(interval=1,
                                                  desired_num_ticks=1,
                                                  num_minor_ticks=0)
            p.yaxis.axis_label = 'Norm. reactivities'
            p.xaxis.ticker = FixedTicker()
            p.grid.grid_line_alpha = 0.6
        else:
            # empty graph
            p = figure()
            p.xaxis.ticker = SingleIntervalTicker(interval=1,
                                                  desired_num_ticks=1,
                                                  num_minor_ticks=0)
            p.yaxis.axis_label = 'Norm. reactivities'
            p.xaxis.ticker = FixedTicker()
            p.grid.grid_line_alpha = 0.6
            p.plot_height = 250
            p.plot_width = 800

        return p

    def comparison_panel(self) -> ComparisonPanel:
        """Return the components of the panel."""

        validated_box = self._make_validated_box()
        metric_button = self._make_metric_button()

        search_input = self._make_texinput()

        score_slider = self._make_slider(name='Replicate fraction [0,1]',
                                         start=0.1,
                                         end=1,
                                         value=0.1,
                                         step=0.1)
        pval_slider = self._make_slider(name='p-value',
                                        start=0.0001,
                                        end=self.pvalue_threshold,
                                        value=0.05,
                                        step=0.0005,
                                        format="0[.]000", )

        self.sub_panels = self._make_subpanels()
        table_selector = self._make_table_selector()
        overview_table = self._make_overview_table()
        label = Div(
            text=f"""
            <div> 
            <style> hr 
            {{ display: block; 
            margin-top: 0.5em; 
            margin-bottom: 0.5em; 
            margin-left: auto; 
            margin-right: auto; 
            border-style: inset; 
            border-width: 1px; }} 
            </style> 
            </div> 
            <hr> 
            Full set v1w3r""")
        self.replicate_selector = self._make_replicate_selector(
            table_selector=table_selector)

        self.graph = self._make_panel_react()

        self.layout = ComparisonPanel(label=label,
                                      validated_box=validated_box,
                                      metric_button=metric_button,
                                      overview_table=overview_table,
                                      score_slider=score_slider,
                                      pvalue_slider=pval_slider,
                                      search_input=search_input,
                                      table_selector=table_selector,
                                      data_source=self.data_source,
                                      table_source=self.table_source,
                                      graph=self.graph,
                                      replicate_sel=self.replicate_selector,
                                      sub_panels=self.sub_panels,
                                      )
        return self.layout

    ##########################################################################
    # end of the initial layout of the panels
    # start the interactive bits
    ##########################################################################

    def __update_react(self, *, transc, ranges):
        """Update the reactivities based on current selections"""

        sequence = read_seq_db(self.col_seqs, transc)
        fn1, fn2, *_ = self.__find_fnames_for_react(transc_id=transc,
                                                    ranges=ranges)
        d_react = self._get_reactivities(transc=transc,
                                         fn1=fn1,
                                         fn2=fn2,
                                         )

        my_box_annotations = []
        for x in self.layout.graph.renderers:
            if isinstance(x, BoxAnnotation) and x.name == "range_rg4":
                x.visible = False
                my_box_annotations.append(x)

        self.layout.data_source.data = dict(x=np.arange(len(d_react[fn1])),
                                            y1=d_react[fn1],
                                            y2=d_react[fn2])
        for_labels = {ii: n for ii, n in enumerate(sequence)}
        pre_labels = {ii: '' for ii in range(-INIT_BOX_PADDING, 0)}
        post_labels = {ii: '' for ii in
                       range(len(sequence), len(sequence) + INIT_BOX_PADDING)}
        self.layout.graph.xaxis.major_label_overrides = {**pre_labels,
                                                         **for_labels,
                                                         **post_labels}
        self.layout.graph.x_range.start = ranges.start - INIT_BOX_PADDING
        self.layout.graph.x_range.end = ranges.end + INIT_BOX_PADDING
        self.layout.graph.x_range.bounds = (-5, len(d_react[fn1]) + 5)
        self.layout.graph.xaxis.ticker = \
            SingleIntervalTicker(interval=1,
                                 desired_num_ticks=1,
                                 num_minor_ticks=0)
        # Finally update the annotations
        my_box_annotations[0].left = ranges.start
        my_box_annotations[0].right = ranges.end
        my_box_annotations[0].fill_alpha = 0.4
        my_box_annotations[0].visible = True

    def __update_graph(self):
        """Update the graphs based on current selections"""

        self.sub_panels = self.layout.sub_panels
        # right now we only care about the first in the selection
        index = \
            next(iter(self.layout.table_source.selected.indices))

        if len(self.layout.table_source.data['transcript']) > 0:
            transc = self.layout.table_source.data['transcript'][index]
            __range = self.layout.table_source.data['range'][index]
            # for a very mysterious reason the namedtuple becomes an array
            # after clicking on the table, and I don't want that
            # i could not figure out where/why the change happens
            __range = Range(*__range)
            self.__update_react(transc=transc, ranges=__range)
        else:
            # present an empty plot
            self.layout.data_source.data = dict(x=[],
                                                y1=[],
                                                y2=[])

    def on_table(self, attr, old, new):
        """React to clicking a row on the table"""
        self.sub_panels = self.layout.sub_panels
        self.__update_replicates()
        self.__update_graph()

    # when you implement the update based on the sub-panels, make
    # sure to place the layout sub-panel to the self.panel
    def __update_replicates(self):
        """Update the list of replicates.

         I do not filter for scoring here, only for pvalues. I assume if
         the transcript has been presented to you from the table_selector
         it means it fulfills the score threshold.

         """
        ref_env = self.sub_panels['Reference'].env_ionic.value.split(" - ")[1]
        ref_ionic = self.sub_panels['Reference']. \
            env_ionic.value.split(" - ")[0]
        ind_env = self.sub_panels['Inducer'].env_ionic.value.split(" - ")[1]
        ind_ionic = self.sub_panels['Inducer'].env_ionic.value.split(" - ")[0]

        index = next(iter(self.layout.table_source.selected.indices))

        transc_id = ""

        if len(self.layout.table_source.data['transcript']) > 0:
            transc_id = self.layout.table_source.data['transcript'][index]

        query = {'transc_id': transc_id,
                 'reference_environment': ref_env,
                 'reference_ionic': ref_ionic,
                 'inducer_environment': ind_env,
                 'inducer_ionic': ind_ionic,
                 'metric': self.metric,
                 }

        match = self.col_signif.find_one(query)
        if match:
            pvalues = self.col_signif.find_one(query)['pvalues']
        else:
            pvalues = []

        list_repl = [str(i + 1) for i, x in enumerate(pvalues)
                     if x < self.pvalue_threshold]

        value_sel = next(iter(list_repl)) if len(list_repl) > 0 else "-"
        self.layout.replicate_sel.title = f"Replicate: {len(list_repl)}"
        self.layout.replicate_sel.value = value_sel
        self.layout.replicate_sel.options = list_repl

    def __filter_transcripts_on_update(self) -> List[transc_range]:
        """Filtering the transcripts based on after an update"""

        ref_env = self.sub_panels['Reference'].env_ionic.value.split(" - ")[1]
        ref_ionic = self.sub_panels['Reference']. \
            env_ionic.value.split(" - ")[0]
        ind_env = self.sub_panels['Inducer'].env_ionic.value.split(" - ")[1]
        ind_ionic = self.sub_panels['Inducer'].env_ionic.value.split(" - ")[0]

        query = {
            'reference_ionic': ref_ionic,
            'reference_environment': ref_env,
            'inducer_ionic': ind_ionic,
            'inducer_environment': ind_env,
            'validated': self.validated,
            'metric': self.metric,
            'pvalues': {'$elemMatch': {'$lte': self.pvalue_threshold}},
            '$expr': {'$gt': [{'$divide': ['$score', '$records_inducer']},
                              self.score_threshold],
                      },
        }
        t_r = []
        for m in self.col_signif.find(query):
            counter = 0
            for p in m['pvalues']:
                if p < self.pvalue_threshold:
                    counter += 1
            if counter / m['records_inducer'] >= self.score_threshold:
                t_r.append(transc_range(m['transc_id'], Range(*m['range']),
                                        m['score'] / m['records_inducer']))

        return sorted(t_r, key=lambda x: x.score, reverse=True)

    def __update_table(self):
        """Update the table based on current selections."""

        list_of_transc = self.__filter_transcripts_on_update()
        new_d = {'transcript': [], 'range': [], 'score': []}
        for l in list_of_transc:
            new_d['transcript'].append(l.transc)
            new_d['range'].append(l.range)
            new_d['score'].append(l.score)

        # it has to be updated all at once
        self.layout.table_source.data = new_d
        self.table_source.data = new_d
        # pre-select the first entry
        self.layout.table_source.selected.indices = [0]
        self.table_source.selected.indices = [0]

    def __update_html_table(self):
        """Repopulate the html table if anything changes

        For the counts to be up to date, you *must* update the
        table of transcripts/ranges first

        """
        transc_n, rg4_n = self.__count_transc_and_rg4s()
        html = make_html_table(transcript_n=transc_n, rg4_n=rg4_n)
        self.layout.overview_table.text = html

    def __top_level_update(self):
        """Common updates for env, metric, sliders and validated"""

        # the transcript list
        self.__update_table()
        # the transcript counts
        self.__update_html_table()
        # the replicate selector
        self.__update_replicates()
        # the graph
        self.__update_graph()

    def search_transc(self, attr, old, new):
        """Search a transc_id and if found select it"""

        logger.debug(f"You have added {new}")
        if new in self.layout.table_source.data['transcript']:
            ind = self.layout.table_source.data['transcript'].index(new)
            self.layout.table_selector.source.selected.indices = [ind]
            self.sub_panels = self.layout.sub_panels
            self.__update_replicates()
            self.__update_graph()

    def update_on_env(self, attr, old, new):
        """Update the list based on environment setting"""

        self.sub_panels = self.layout.sub_panels
        self.__top_level_update()

    def update_on_replicate(self, attr, old, new):
        """Update graph only, not the conditions"""
        self.sub_panels = self.layout.sub_panels
        self.__update_graph()

    def update_on_metric(self, new):
        """Update everything if the user changes the metric"""

        try:
            self.layout.metric = OPT_TO_METRIC[new]
            self.metric = OPT_TO_METRIC[new]
        except KeyError as e:
            logger.warning(f"Option {new} not in my metric records")
            logger.warning(f"I bet you forgot to add a new option.")
            logger.warning(f"Defaulting to rmsd")
            self.layout.metric = 'rmsd'

        self.__top_level_update()

    def update_on_validated(self, new):
        """Update everything if the user changes the metric"""
        if 0 in new:
            self.layout.validated = True
            self.validated = True
        else:
            self.layout.validated = {'$exists': True}
            self.validated = {'$exists': True}

        self.__top_level_update()

    def update_on_pval_slider(self, _, __, new):
        """Update transcripts based on the slider value"""

        self.pvalue_threshold = self.layout.pvalue_slider.value
        self.__top_level_update()

    def update_on_score_slider(self, _, __, new):
        """Update transcripts based on the slider value"""

        self.score_threshold = self.layout.score_slider.value
        self.__top_level_update()

    def update_ticker_start(self, _, __, new):
        """Change the ticker to avoided crowding"""
        if self.layout.graph.x_range.end - new > 90:
            self.layout.graph.xaxis.ticker = FixedTicker()
        else:
            self.layout.graph.xaxis.ticker = \
                SingleIntervalTicker(interval=1,
                                     desired_num_ticks=1,
                                     num_minor_ticks=0)

    def update_ticker_end(self, _, __, new):
        """Change the ticker to avoid crowding"""
        if new - self.layout.graph.x_range.start > 90:
            self.layout.graph.xaxis.ticker = FixedTicker()
        else:
            self.layout.graph.xaxis.ticker = \
                SingleIntervalTicker(interval=1,
                                     desired_num_ticks=1,
                                     num_minor_ticks=0)
