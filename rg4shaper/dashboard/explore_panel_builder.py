"""Refactoring."""

import logging
import math
import pickle
from collections import namedtuple
from typing import List, NamedTuple

import attr
import numpy as np
from bokeh.models import Slider, Div, Select, ColumnDataSource, HoverTool, \
    TableColumn, DataTable, Range1d, BoxAnnotation, FixedTicker, \
    SingleIntervalTicker
from bokeh.plotting import figure

from ..compare_conditions import read_seq_db
from ..document_classes import Range

react_pair = namedtuple('Reactivities',
                        ['first', 'second'])

logger = logging.getLogger('rg4_logger')

INIT_BOX_PADDING = 25
OPT_TO_METRIC = {0: 'gini', 1: 'rmsd'}

ENV_OPT = {'vivo': ['K', 'KPDS', 'KcPDS', 'KPhenDC3'],
           'vitro': ['Li', 'K', 'KPDS', 'KcPDS']
           }


@attr.s
class SubPanel(object):
    label = attr.ib()
    environment_sel = attr.ib()
    ionic_sel = attr.ib()
    replicate_sel = attr.ib()


@attr.s
class ExpComparisonPanel(object):
    """Store panel info"""
    label: str = attr.ib()
    rts_slider = attr.ib()
    pvalue_slider = attr.ib()
    table_selector = attr.ib()
    table_source = attr.ib()
    graph = attr.ib()
    data_source: ColumnDataSource = attr.ib()
    sub_panels: List[SubPanel] = attr.ib(default=attr.Factory(list))


transc_range = namedtuple("Transc_range", ['transc', 'range', 'rts', 'pvalue'])


class PanelExplorer(object):

    def __init__(self, *, cntrl_type, col_dataset, col_react, col_seqs,
                 rts_threshold, pvalue_threshold, col_cntrl):
        """Initialize"""
        self.cntrl_type = cntrl_type
        self.col_dataset = col_dataset
        self.col_react = col_react
        self.col_seqs = col_seqs
        self.col_cntrl = col_cntrl
        self.rts_threshold = rts_threshold
        self.pvalue_threshold = pvalue_threshold
        self.panels = None
        self.layout = None
        self.current_transc = None
        self.sub_panels = None
        self.data_source = ColumnDataSource(data={'x': np.zeros(1),
                                                  'y1': np.zeros(1),
                                                  'y2': np.zeros(1), },
                                            )
        self.table_source = ColumnDataSource(data={'transcript': [],
                                                   'range': [],
                                                   'rts_value': [],
                                                   'pvalue': [],
                                                   })
        self.columns_table = [
            TableColumn(field='transcript', title='Transcript'),
            TableColumn(field='range', title='Range'),
            TableColumn(field='rts_value', title='RTS value'),
            TableColumn(field='pvalue', title='p-value'),
        ]
        hover = HoverTool(
            tooltips=[
                ("Base (0-based)", "$index"),
                ("Norm. reactivity", "$y"),
            ],
            mode='mouse')

        self.tools = ['pan', 'xwheel_zoom', 'xbox_zoom', hover, ]

    def __enter__(self):
        self.panels = self.comparison_panel()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        logger.debug(f"Done building exploratory panel")
        pass

    def _make_slider(self, *, name: str, start: float, end: float,
                     value: float,
                     step: float):
        """Make a slider"""
        w = \
            Slider(start=start, end=end, value=value, step=step,
                   title=name)
        return w

    def __filter_transcripts(self) -> List[NamedTuple]:
        """Filtering the transcripts based on thresholds"""

        query = {'control_type': self.cntrl_type,
                 'p_value': {'$lt': self.pvalue_threshold},
                 'rts_value': {'$gt': self.rts_threshold},
                 }
        t_r = []
        for m in self.col_cntrl.find(query):
            t_r.append(transc_range(m['transc_id'], Range(*m['range']),
                                    m['rts_value'], m['p_value']))

        return sorted(t_r, key=lambda x: x.rts, reverse=True)

    def _make_table_selector(self):
        """Create transcript table"""
        list_of_transc = self.__filter_transcripts()
        for l in list_of_transc:
            self.table_source.data['transcript'].append(l.transc)
            self.table_source.data['range'].append(l.range)
            self.table_source.data['rts_value'].append(l.rts)
            self.table_source.data['pvalue'].append(l.pvalue)
        w = \
            DataTable(source=self.table_source, columns=self.columns_table)
        # pre-select the first entry
        w.source.selected = {
            '0d': {'glyph': None, 'get_view': {}, 'indices': []},
            '1d': {'indices': [0]}, '2d': {'indices': {}}}
        return w

    def _get_reactivities(self, *, transc, fn1, fn2):
        """Return the reactivities"""
        react = []
        for fn in [fn1, fn2]:
            query = {'fname': fn,
                     'transc_id': transc
                     }
            react.append(
                pickle.loads(self.col_react.find_one(query)['reactivities']))

        return react

    def _make_subpanels(self):
        """Build two subpanels"""
        sub_panels = []
        for _ in range(2):
            environment_sel = Select(title="Environment",
                                     value=[x for x in ENV_OPT.keys()][_],
                                     options=[x for x in ENV_OPT.keys()])
            ionic_sel = Select(title="Ionic medium",
                               value=ENV_OPT[environment_sel.value][_],
                               options=ENV_OPT[environment_sel.value],
                               )

            env = environment_sel.value
            ionic = ionic_sel.value
            query = {'cell_type': 'Hela',
                     'environment': env,
                     'ionic': ionic}
            num_repl = self.col_dataset.find_one(query)['replicates']
            list_repl = [str(x) for x in range(1, num_repl + 1)]

            replicate_sel = Select(title=f"Replicate: {num_repl}",
                                   value=next(iter(list_repl)),
                                   options=list_repl)

            label = Div(
                text=f"""
            <div> 
            <style> hr 
            {{ display: block; 
            margin-top: 0.5em; 
            margin-bottom: 0.5em; 
            margin-left: auto; 
            margin-right: auto; 
            border-style: inset; 
            border-width: 1px; }} 
            </style> 
            </div> 
            Condition {_+1} 
            <hr> """)
            sub_panels.append(SubPanel(label,
                                       environment_sel,
                                       ionic_sel,
                                       replicate_sel))
        return sub_panels

    def __find_fnames_for_react(self) -> List[str]:
        """Return the filenames according to the condition subpannels"""
        fnames = []
        for sp in self.sub_panels:
            env = sp.environment_sel.value
            ionic = sp.ionic_sel.value
            replicate_n = int(sp.replicate_sel.value) - 1  # we present [1,...]

            query = {'cell_type': 'Hela',
                     'environment': env,
                     'ionic': ionic}

            m = self.col_dataset.find_one(query)
            try:
                fnames.append(m['fname'][replicate_n])
            except KeyError as e:
                logger.error(f"fname is not a key in m")
                sys.exit(1)

        return fnames

    def _make_panel_react(self):
        """Build the graph

        Initially use the first element in the table to show the data

        """
        transc = next(iter(self.table_source.data['transcript']))
        sequence = read_seq_db(self.col_seqs, transc)
        fn1, fn2, *_ = self.__find_fnames_for_react()
        r1, r2, *_ = self._get_reactivities(transc=transc,
                                            fn1=fn1,
                                            fn2=fn2,
                                            )

        ranges = next(iter(self.table_source.data['range']))

        p = figure(tools=self.tools,
                   x_range=Range1d(start=ranges.start - INIT_BOX_PADDING,
                                   end=ranges.end + INIT_BOX_PADDING,
                                   bounds=(-5, len(r1) + 5),
                                   ))
        self.data_source.data = {'x': np.arange(len(r1)), 'y1': r1,
                                 'y2': r2}
        p.line('x', 'y1',
               source=self.data_source,
               color="firebrick",
               legend='Condition 1',
               line_width=2)
        p.line('x', 'y2',
               source=self.data_source,
               color="navy",
               legend='Condition 2',
               line_width=2)

        p.renderers.extend([BoxAnnotation(left=ranges.start,
                                          right=ranges.end,
                                          fill_alpha=0.4,
                                          name="range_rg4")])
        p.plot_height = 250
        p.plot_width = 800
        for_labels = {ii: n for ii, n in enumerate(sequence)}
        pre_labels = {ii: '' for ii in range(-INIT_BOX_PADDING, 0)}
        post_labels = {ii: '' for ii in
                       range(len(sequence), len(sequence) + INIT_BOX_PADDING)}
        p.xaxis.major_label_overrides = {**pre_labels, **for_labels,
                                         **post_labels}
        p.xaxis.major_label_orientation = math.pi / 4
        p.xaxis.ticker = SingleIntervalTicker(interval=1,
                                              desired_num_ticks=1,
                                              num_minor_ticks=0)
        p.yaxis.axis_label = 'Norm. reactivities'
        p.xaxis.ticker = FixedTicker()
        p.grid.grid_line_alpha = 0.6
        return p

    def comparison_panel(self):
        """Return the components of the panel"""

        rts_slider = self._make_slider(name='RTS threshold',
                                       start=0.0,
                                       end=1.0,
                                       value=self.rts_threshold,
                                       step=0.001)

        pval_slider = self._make_slider(name='p-value',
                                        start=0.0001,
                                        end=self.pvalue_threshold,
                                        value=0.05,
                                        step=0.0005)

        table_selector = self._make_table_selector()
        label = Div(
            text=f"""
            <div> 
            <style> hr 
            {{ display: block; 
            margin-top: 0.5em; 
            margin-bottom: 0.5em; 
            margin-left: auto; 
            margin-right: auto; 
            border-style: inset; 
            border-width: 1px; }} 
            </style> 
            </div> 
            <hr> 
            Compared conditions {self.cntrl_type} """)
        self.sub_panels = self._make_subpanels()

        self.graph = self._make_panel_react()

        self.layout = ExpComparisonPanel(label=label,
                                         rts_slider=rts_slider,
                                         pvalue_slider=pval_slider,
                                         table_selector=table_selector,
                                         data_source=self.data_source,
                                         table_source=self.table_source,
                                         graph=self.graph,
                                         sub_panels=self.sub_panels,
                                         )
        return self.layout

    def __update_react(self, *, transc, ranges):
        """Update the reactivities based on current believes"""
        sequence = read_seq_db(self.col_seqs, transc)
        fn1, fn2, *_ = self.__find_fnames_for_react()
        r1, r2, *_ = self._get_reactivities(transc=transc,
                                            fn1=fn1,
                                            fn2=fn2,
                                            )
        # Would have been nice to change the legends without redoing
        # the plot, but i can't get it to work now...
        # cond_1 = self.sub_panels[0].environment_sel.value + " " + \
        #          self.sub_panels[0].ionic_sel.value
        # cond_2 = self.sub_panels[1].environment_sel.value + " " + \
        #          self.sub_panels[1].ionic_sel.value

        my_box_annotations = []
        for x in self.layout.graph.renderers:
            if isinstance(x, BoxAnnotation) and x.name == "range_rg4":
                x.visible = False
                my_box_annotations.append(x)

        self.layout.data_source.data = dict(x=np.arange(len(r1)),
                                            y1=r1,
                                            y2=r2)
        for_labels = {ii: n for ii, n in enumerate(sequence)}
        pre_labels = {ii: '' for ii in range(-INIT_BOX_PADDING, 0)}
        post_labels = {ii: '' for ii in
                       range(len(sequence), len(sequence) + INIT_BOX_PADDING)}
        self.layout.graph.xaxis.major_label_overrides = {**pre_labels,
                                                         **for_labels,
                                                         **post_labels}
        self.layout.graph.x_range.start = ranges.start - INIT_BOX_PADDING
        self.layout.graph.x_range.end = ranges.end + INIT_BOX_PADDING
        self.layout.graph.x_range.bounds = (-5, len(r1) + 5)
        self.layout.graph.xaxis.ticker = \
            SingleIntervalTicker(interval=1,
                                 desired_num_ticks=1,
                                 num_minor_ticks=0)
        # Finally update the annotations
        my_box_annotations[0].left = ranges.start
        my_box_annotations[0].right = ranges.end
        my_box_annotations[0].fill_alpha = 0.4
        my_box_annotations[0].visible = True

    def __update_graph(self):
        """Update the graphs based on current selections"""
        self.sub_panels = self.layout.sub_panels
        # right now we only care about the first in the selection
        index = \
            next(iter(self.layout.table_source.selected['1d']['indices']))
        transc = self.table_source.data['transcript'][index]
        __range = self.table_source.data['range'][index]
        self.__update_react(transc=transc, ranges=__range)

    def on_table(self, attr, old, new):
        """React to clicking a row on the table"""
        self.sub_panels = self.layout.sub_panels
        self.__update_graph()

    # HEI!!! WHEN YOU IMPLEMENT THE UPDATE BASED ON THE SUBPANELS, MAKE
    # SURE TO PLACE THE LAYOUT SUBPANEL TO THE SELF.PANEL
    def __update_replicates(self):
        """Update the list of replicates

        It will update all no matter which panel was updated, which is
        easier to implement. If it turns out to be a drag, add a selector
        for each.
        """
        for sp in self.sub_panels:
            env = sp.environment_sel.value
            ionic = sp.ionic_sel.value
            query = {'cell_type': 'Hela',
                     'environment': env,
                     'ionic': ionic}
            num_repl = self.col_dataset.find_one(query)['replicates']
            list_repl = [str(x) for x in range(1, num_repl + 1)]
            sp.replicate_sel.title = f"Replicate: {num_repl}"
            sp.replicate_sel.value = next(iter(list_repl))
            sp.replicate_sel.options = list_repl

    def update_on_env(self, attr, old, new):
        """Update the list based on environment setting"""
        self.sub_panels = self.layout.sub_panels
        for sp in self.sub_panels:
            env = sp.environment_sel.value
            sp.ionic_sel.value = ENV_OPT[env][0]
            sp.ionic_sel.options = ENV_OPT[env]
        self.__update_replicates()
        self.__update_graph()

    def update_on_ionic(self, attr, old, new):
        """Update the all subpanels"""
        self.sub_panels = self.layout.sub_panels
        self.__update_replicates()
        self.__update_graph()

    def update_on_replicate(self, attr, old, new):
        """Update graph only, not the conditions"""
        self.sub_panels = self.layout.sub_panels
        self.__update_graph()

    def update_ticker_start(self, _, __, new):
        """Change the ticker to avoided crowding"""
        if self.layout.graph.x_range.end - new > 90:
            self.layout.graph.xaxis.ticker = FixedTicker()
        else:
            self.layout.graph.xaxis.ticker = \
                SingleIntervalTicker(interval=1,
                                     desired_num_ticks=1,
                                     num_minor_ticks=0)

    def update_ticker_end(self, _, __, new):
        """Change the ticker to avoid crowding"""
        if new - self.layout.graph.x_range.start > 90:
            self.layout.graph.xaxis.ticker = FixedTicker()
        else:
            self.layout.graph.xaxis.ticker = \
                SingleIntervalTicker(interval=1,
                                     desired_num_ticks=1,
                                     num_minor_ticks=0)
