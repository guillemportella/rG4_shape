"""Build the dashboard for the comparisons across in vivo comparisons."""
import logging
import time
from functools import partial

from bokeh.layouts import layout
from pymongo import MongoClient

from . import react_panel_builder as rpb
from ..load_config import config, read_user_config_viewer

MONGO_HOST_AND_PORT = config.rg4_react['MONGO_PORT_AND_HOST']
COL_SEQS_NAME = config.dashboard['COLLECTION_SEQS']
DB_NAME = config.dashboard['MONGODB_DATABASE_NAME']
COLLECTION_REACT_FN = config.dashboard['COLLECTION_RAW_REACTIVITIES']
COLLECTION_SIG_FN = config.dashboard[
    'COLLECTION_RANGES_DIFF_CONDITIONS']

logger = logging.getLogger('rg4_logger')


class DashBoardDriverVivo(object):
    """Specialized class to draw Dashboard based on the inv ivo results"""

    def __init__(self, *, db_name=None, col_sig: object = None,
                 col_react: object = None, col_seqs_name=None):
        self.col_seqs_name = col_seqs_name
        self.db_name = db_name
        self.col_sig_name = col_sig
        self.col_react_name = col_react
        # ONLY FOR TESTING
        self.vivo_conditions = []
        self.score_threshold = 0.1
        self.pvalue_threshold = 0.05  # max value
        self.validated = True
        self.metric = 'gini'
        self.transc_ids = {}

    def __enter__(self):
        """Open the connection"""
        client = MongoClient(host=[MONGO_HOST_AND_PORT])
        db = client[self.db_name]
        self.start_t = time.clock()
        self.col_sig = db[self.col_sig_name]
        self.col_react = db[self.col_react_name]
        self.col_seqs = db[self.col_seqs_name]
        self._setup_conditions()
        return self

    def _setup_conditions(self) -> None:
        """Initialize the panels"""

        query = {'validated': {'$exists': True},
                 'inducer': {'$exists': True},
                 'react_pairs': {'$exists': True},
                 'metric': self.metric
                 }

        self.vivo_conditions = self.col_sig.distinct('inducer', query)

        for inducer in self.vivo_conditions:
            cursor = rpb.find_documents(threshold=self.score_threshold,
                                        col_sig=self.col_sig,
                                        inducer=inducer,
                                        metric=self.metric,
                                        validated=self.validated)
            for c in cursor:
                self.transc_ids.setdefault(inducer, []).append(c['transc_id'])

    def dashboard(self):
        """Place the dashboards, return the number of panels placed.
        Returns

        -------
        layouts - dict of layouts, the keys are the comparison conditions
        """
        new_layout = {}
        for ionic_condition in self.vivo_conditions:
            logger.debug(f"Conditions {ionic_condition}")
            with rpb.PanelComparisonVivo(col_seqs=self.col_seqs,
                                         col_sig=self.col_sig,
                                         col_react=self.col_react,
                                         score_threshold=self.score_threshold,
                                         pval_threshold=self.pvalue_threshold,
                                         validated=self.validated,
                                         metric=self.metric,
                                         inducer=ionic_condition,
                                         transc=self.transc_ids[
                                             ionic_condition]) as pcomp:
                new_layout[ionic_condition] = pcomp.panels

                # Add the controllers
                new_layout[ionic_condition].validated_checkbox. \
                    on_click(pcomp.update_on_validated),

                new_layout[ionic_condition].metric. \
                    on_click(pcomp.update_on_metric)

                new_layout[ionic_condition].score_slider. \
                    on_change("value", pcomp.update_on_slider)

                new_layout[ionic_condition].pval_filter. \
                    on_change("value", pcomp.update_on_pvalue),

                new_layout[ionic_condition].transc_selector. \
                    on_change("value", pcomp.update_on_transc),

                new_layout[ionic_condition].range_selector. \
                    on_change("value", pcomp.update_on_ranges)

                new_layout[ionic_condition].react_selector. \
                    on_change("value", pcomp.update_graph)

                new_layout[ionic_condition].graph.x_range. \
                    on_change("start", pcomp.update_ticker_start)

        return new_layout

    def __exit__(self, exc_type, exc_val, exc_tb):
        """A silly little check so far."""
        logger.debug("Finished execution in {0:.2f} seconds.".format(
            time.clock() - self.start_t))


def serve_react_vivo(*, conf_file):
    """Return a reactivities dashboard for in vivo results
    :param control_type:
    """

    def react_vivo(doc, *, conf_file):
        """Return the doc after placing the dashboard elements"""
        if conf_file is not None:
            read_user_config_viewer(logger=logger, user_config_fn=conf_file)
        with DashBoardDriverVivo(db_name=DB_NAME,
                                 col_sig=COLLECTION_SIG_FN,
                                 col_seqs_name=COL_SEQS_NAME,
                                 col_react=COLLECTION_REACT_FN) as db:
            to_layout = []
            for v in db.dashboard().values():
                to_layout.extend([[v.label]])
                to_layout.extend([[v.validated_checkbox,
                                   v.metric]])
                to_layout.extend([[[[v.score_slider], [v.pval_filter]
                                    ], [v.overview_table]]])
                to_layout.extend([[v.transc_selector]])
                to_layout.extend([[v.range_selector,
                                   v.react_selector]]),
                to_layout.extend([[v.graph]])
                to_layout.extend([[v.graph_metric]])

        document = layout(to_layout, sizing_mode='scale_width')
        doc.add_root(document)
        logger.debug("Brilliant!")
        doc.title = "GPC_rG4_vivo"

    return partial(react_vivo, conf_file=conf_file)
