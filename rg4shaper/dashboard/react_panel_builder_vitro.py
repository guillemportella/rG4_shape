import logging
import pickle
from collections import namedtuple
from math import pi
from typing import NamedTuple, Any

import attr
import numpy as np
from bokeh.models import ColumnDataSource, Range1d, BoxAnnotation, HoverTool, \
    SingleIntervalTicker, FixedTicker
from bokeh.models.widgets import Select, Div
from bokeh.plotting import figure

from ..compare_conditions import read_seq_db
from ..document_classes import Range

react_pair_bounds = namedtuple('Reactivities_bounds',
                               ['first', 'second', 'ranges'])
react_pair = namedtuple('Reactivities',
                        ['first', 'second'])

logger = logging.getLogger('rg4_logger')
INIT_BOX_PADDING = 25


@attr.s
class VitroComparisonPanel(object):
    """store panels"""
    label: str = attr.ib()
    replicates = attr.ib()
    transcripts = attr.ib()
    graph = attr.ib()
    data_source = attr.ib()


class PanelComparison(object):
    """Select replicates and transcripts"""

    def __init__(self, *, repl: int, comp_name: str,
                 col_sig, col_react, tr_dict: dict, col_seqs, rep2id: dict):
        self.replicates = repl
        self.transcripts = tr_dict
        self.comparison = comp_name
        self.layout = None
        self.col_sig = col_sig
        self.col_react = col_react
        self.col_seqs = col_seqs
        self.rep2id = rep2id

        self.data_source = ColumnDataSource(data={'x': np.zeros(1),
                                                  'y1': np.zeros(1),
                                                  'y2': np.zeros(1), })

        hover = HoverTool(
            tooltips=[
                ("Base (0-based)", "$index"),
                ("Norm. reactivity", "$y"),
            ],
            mode='mouse')

        self.tools = ['pan', 'xwheel_zoom', 'xbox_zoom', hover, ]

    def __enter__(self):
        self.panels = self.comparison_panel()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        logger.debug("Done!")

    def _make_panel_replicate(self):
        """Widget w replicates"""
        the_options = [str(x) for x in range(1, self.replicates + 1)]
        w = Select(title=f"Replicates: {len(the_options)} ",
                   value="1",
                   options=the_options)

        return w

    def _make_panel_transcript(self):
        """Widget with transcripts"""
        w = Select(title="Transcripts",
                   value=list(self.transcripts[0].keys())[0],
                   options=list(self.transcripts[0].keys()))

        return w

    def _make_panel_react(self):
        """Create a panel with the reactivities."""

        init_transc = next(iter(self.transcripts[0]))
        ranges = self.transcripts[0][init_transc]
        sequence = read_seq_db(self.col_seqs, init_transc)
        # Find the file reactivities
        dbase_id = self.rep2id[0]
        react_1, react_2 = get_reactivities(
            transc_name=init_transc,
            comp_name=self.comparison,
            dbase_id=dbase_id,
            col_sig=self.col_sig,
            col_react=self.col_react,
        )

        cond_1, cond_2 = self.comparison.split("-")
        p = figure(tools=self.tools,
                   x_range=Range1d(start=-5,
                                   end=len(react_1) + 5,
                                   bounds=(-5, len(react_1) + 5),
                                   ))
        self.data_source.data = {'x': np.arange(len(react_1)), 'y1': react_1,
                                 'y2': react_2}
        p.line('x', 'y1',
               source=self.data_source,
               color="firebrick",
               legend=cond_1,
               line_width=2)
        p.line('x', 'y2',
               source=self.data_source,
               color="navy",
               legend=cond_2,
               line_width=2)

        for rr in ranges:
            p.renderers.extend([BoxAnnotation(left=rr.start, right=rr.end,
                                              fill_alpha=0.4,
                                              name="range_rg4")])
        p.plot_height = 250
        p.plot_width = 800
        for_labels = {ii: n for ii, n in enumerate(sequence)}
        pre_labels = {ii: '' for ii in range(-INIT_BOX_PADDING, 0)}
        post_labels = {ii: '' for ii in
                       range(len(sequence), len(sequence) + INIT_BOX_PADDING)}
        p.xaxis.major_label_overrides = {**pre_labels, **for_labels,
                                         **post_labels}
        p.xaxis.major_label_overrides = for_labels
        p.xaxis.major_label_orientation = pi / 4
        p.xaxis.ticker = FixedTicker()
        p.xgrid[0].ticker.desired_num_ticks = 40
        p.grid.grid_line_alpha = 0.6
        return p

    def comparison_panel(self) -> VitroComparisonPanel:
        """Return replicate, transcripts and graph panels."""

        replicate_panel = self._make_panel_replicate()
        transcripts_panel = self._make_panel_transcript()
        # Start by showing the transcript of the first replicate
        reactivities_panel = self._make_panel_react()
        label = Div(
            text=f"""
            <div> 
            <style> hr 
            {{ display: block; 
            margin-top: 0.5em; 
            margin-bottom: 0.5em; 
            margin-left: auto; 
            margin-right: auto; 
            border-style: inset; 
            border-width: 1px; }} 
            </style> 
            </div> 
            <hr> 
            Compared conditions {self.comparison}""")

        self.layout = VitroComparisonPanel(label=label,
                                           replicates=replicate_panel,
                                           transcripts=transcripts_panel,
                                           graph=reactivities_panel,
                                           data_source=self.data_source,
                                           )

        return self.layout

    def update_on_transcript(self, _, __, new):
        """Update the list of transcripts based on the replicate selected."""
        # Now update the graph
        self.update_graph(attr, "", "1")

    def update_graph(self, _, __, new):
        """Update the graph if a change of transcript is detected."""
        transc_name = self.layout.transcripts.value
        logger.debug(f"Lent of tr is {len(self.transcripts)}")
        logger.debug(f"{self.transcripts[4]}")
        ranges = self.transcripts[int(new) - 1][transc_name]
        dbase_id = self.rep2id[0]
        react_1, react_2 = \
            get_reactivities(transc_name=transc_name,
                             comp_name=self.comparison,
                             dbase_id=dbase_id,
                             col_sig=self.col_sig,
                             col_react=self.col_react,
                             )

        # A bit of a hack. As of today, BoxAnnotations can not be
        # removed, i.e. using fig.renderers.remove(object).
        # https://goo.gl/CbH3Mv
        # I managed to kick them out of renderer, but they still linger in the
        # figure. So the only way is to keep a dynamic list, switching the
        # visibility on/off when needed. More overhead...

        logger.debug("Ok, got reactivities")

        annotations = []
        for x in self.layout.graph.renderers:
            if isinstance(x, BoxAnnotation) and x.name == "range_rg4":
                x.visible = False
                annotations.append(x)
        self.layout.data_source.data = dict(x=np.arange(len(react_1)),
                                            y1=react_1,
                                            y2=react_2)
        sequence = read_seq_db(self.col_seqs, transc_name)
        logger.debug("Ok, got seq")
        for_labels = {ii: n for ii, n in enumerate(sequence)}
        pre_labels = {ii: '' for ii in range(-INIT_BOX_PADDING, 0)}
        post_labels = {ii: '' for ii in
                       range(len(sequence), len(sequence) + INIT_BOX_PADDING)}
        self.layout.graph.xaxis.major_label_overrides = {**pre_labels,
                                                         **for_labels,
                                                         **post_labels}

        self.layout.graph.x_range.end = len(react_1) + 5
        self.layout.graph.x_range.bounds = (-5, len(react_1) + 5)
        logger.debug("What now??")
        # Add missing BoxAnnotations if the number of ranges is larger
        if len(ranges) > len(annotations):
            annotations.extend(
                [BoxAnnotation(visible=False,
                               fill_alpha=0.4,
                               name='range_rg4') for _ in
                 range(len(annotations), len(ranges))])
        # Finally update the annotations
        for i, rr in enumerate(ranges):
            annotations[i].left = rr.start
            annotations[i].right = rr.end
            annotations[i].fill_alpha = 0.4
            annotations[i].visible = True
            self.layout.graph.renderers.extend(
                [BoxAnnotation(left=rr.start, right=rr.end,
                               name="range_rg4")])
        logger.debug("Done with update")

    def update_ticker_start(self, _, __, new):
        """Change the ticker to avoided crowding"""
        if self.layout.graph.x_range.end - new > 90:
            self.layout.graph.xaxis.ticker = FixedTicker()
        else:
            self.layout.graph.xaxis.ticker = \
                SingleIntervalTicker(interval=1,
                                     desired_num_ticks=1,
                                     num_minor_ticks=0)

    def update_ticker_end(self, _, __, new):
        """Change the ticker to avoid crowding"""
        if new - self.layout.graph.x_range.start > 90:
            self.layout.graph.xaxis.ticker = FixedTicker()
        else:
            self.layout.graph.xaxis.ticker = \
                SingleIntervalTicker(interval=1,
                                     desired_num_ticks=1,
                                     num_minor_ticks=0)


def get_reactivities_and_ranges(*, dbase_id,
                                transc_name: str,
                                comp_name: str,
                                col_sig: Any,
                                col_react: Any) -> NamedTuple:
    """Return np.array of reactivities."""
    cond_1, cond_2 = comp_name.split("-")
    matches = col_sig.find_one({"_id": dbase_id})
    fn_1 = matches['reactivities'][cond_1]
    fn_2 = matches['reactivities'][cond_2]
    query_react_1 = {'fname': fn_1, 'transc_id': transc_name}
    query_react_2 = {'fname': fn_2, 'transc_id': transc_name}
    the_ranges = [Range(*x) for x in
                  matches['conditions_diff_ranges'][comp_name][
                      transc_name]]
    match_1 = col_react.find_one(query_react_1)
    match_2 = col_react.find_one(query_react_2)
    react_1 = pickle.loads(match_1['reactivities'])
    react_2 = pickle.loads(match_2['reactivities'])

    return react_pair_bounds(react_1, react_2, the_ranges)


def get_reactivities(*, dbase_id,
                     transc_name: str,
                     comp_name: str,
                     col_sig: Any,
                     col_react: Any) -> NamedTuple:
    """Return np.array of reactivities."""
    cond_1, cond_2 = comp_name.split("-")
    matches = col_sig.find_one({"_id": dbase_id})
    fn_1 = matches['reactivities'][cond_1]
    fn_2 = matches['reactivities'][cond_2]
    query_react_1 = {'fname': fn_1, 'transc_id': transc_name}
    query_react_2 = {'fname': fn_2, 'transc_id': transc_name}
    match_1 = col_react.find_one(query_react_1)
    match_2 = col_react.find_one(query_react_2)
    react_1 = pickle.loads(match_1['reactivities'])
    react_2 = pickle.loads(match_2['reactivities'])

    return react_pair(react_1, react_2)
