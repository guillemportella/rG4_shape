"""Calculate consensus regions between replicates."""
import logging

from pymongo import MongoClient

import rg4shaper.document_classes as order
from rg4shaper.load_config import config

logger = logging.getLogger('rg4_logger')

MONGO_HOST_AND_PORT = config.rg4_react['MONGO_PORT_AND_HOST']


def consensus_rg4(args) -> int:
    """Calculate consensus regions."""
    db_name = args['db']
    collection_name = args['cn']
    b_clean_consensus = args['clean_consensus']
    client = MongoClient(host=[MONGO_HOST_AND_PORT])
    db = client[db_name]
    collection = db[collection_name]

    if b_clean_consensus:
        logger.info(f"Deleting consensus records from {collection_name}")
        to_del = collection.delete_many({'is_consensus': True})
        logger.info(f"Deleted {to_del.deleted_count} records")
        return 1

    for inducer in ['cPDS', 'PDS']:

        matched_experiments = collection.find(
            {
                'cell_type': 'Hela',
                'environment': 'vitro',
                'g4_inducer': inducer,
                'is_consensus': {"$exists": False}
            }
        )

        cons_doc = order.ConsensusRG4(
            cell_type='Hela',
            environment='vitro',
            g4_inducer=inducer
        )

        for doc in matched_experiments:
            try:
                for condition in doc['conditions_diff_ranges']:
                    cons_doc.aggregate_consensus(
                        results_dict=doc['conditions_diff_ranges'],
                        compared_condition=condition,
                    )

            except KeyError:
                raise KeyError

        collection.insert_one(vars(cons_doc))

    return 1
