import configparser
import logging.config
import sys
from collections import namedtuple

from pkg_resources import resource_filename

MAX_ALLOWED_WORKERS = 100
MAX_PROCESS_WORKERS = 4
MONGO_HOST_AND_PORT = "localhost:27017"

external_config = configparser.ConfigParser()
filename = resource_filename(__name__,
                             "conf/rg4_react.conf")
external_config.read(filename)

rg4_react = {}
rg4_react['MONGO_PORT_AND_HOST'] = external_config['DEFAULT']['HOST_AND_PORT']
rg4_react['DEBUG'] = external_config['DEFAULT']['DEBUG']
rg4_react['MAX_PROCESS_WORKERS'] = external_config['DEFAULT'][
    'MAX_PROCESS_WORKERS']

dashboard = {}
dashboard['COLLECTION_SEQS'] = external_config['DASHBOARD']['COLLECTION_SEQS']
dashboard['COLLECTION_SEQS_FN'] = external_config['DASHBOARD'][
    'COLLECTION_SEQS_FN']
dashboard['COLLECTION_SIGNIFICANT_FN'] = external_config['DASHBOARD'][
    'COLLECTION_SIGNIFICANT_FN']
dashboard['COLLECTION_DATASET_FN'] = external_config['DASHBOARD'][
    'COLLECTION_DATASET_FN']
dashboard['MONGODB_DATABASE_NAME'] = \
    external_config['DASHBOARD']['MONGODB_DATABASE_NAME']
dashboard['COLLECTION_RANGES_DIFF_CONDITIONS'] = \
    external_config['DASHBOARD']['COLLECTION_RANGES_DIFF_CONDITIONS']
dashboard['COLLECTION_FALSE_NEGATIVES'] = \
    external_config['DASHBOARD']['COLLECTION_FALSE_NEGATIVES']
dashboard['COLLECTION_TRUE_POSITIVES'] = \
    external_config['DASHBOARD']['COLLECTION_TRUE_POSITIVES']
dashboard['COLLECTION_RAW_REACTIVITIES'] = \
    external_config['DASHBOARD']['COLLECTION_RAW_REACTIVITIES']


# logger = logging.getLogger('rg4_logger')

def read_user_config_viewer(*, logger, user_config_fn: str):
    """Read in the user  configuration for react viewer"""
    # COLLECTION_SIG_FN = config.dashboard[
    # 'COLLECTION_RANGES_DIFF_CONDITIONS']
    user_config = configparser.ConfigParser()
    user_config.read_file(open(user_config_fn))
    user_config.read(user_config_fn)

    try:
        global MONGO_HOST_AND_PORT
        MONGO_HOST_AND_PORT = user_config['DEFAULT']['HOST_AND_PORT']
    except KeyError as e:
        msg = "Could not find HOST_AND_PORT in the DEFAULT section "
        msg += f"of file {user_config_fn}."
        logger.warning(msg)
        sys.exit(0)

    try:
        global COL_SEQS_FN
        COL_SEQS_FN = user_config['DEFAULT']['COLLECTION_SEQS']
    except KeyError as e:
        msg = "Could not find COL_SEQS_FN in the DEFAULT section "
        msg += f"of file {user_config_fn}."
        logger.warning(msg)
        sys.exit(0)

    try:
        global DB_NAME
        DB_NAME = user_config['DASHBOARD']['MONGODB_DATABASE_NAME']
    except KeyError as e:
        msg = "Could not find MONGODB_DATABASE_NAME in the DASHBOARD section "
        msg += f"of file {user_config_fn}."
        logger.warning(msg)
        sys.exit(0)

    try:
        global COLLECTION_REACT_FN
        COLLECTION_REACT_FN = \
            user_config['DASHBOARD']['COLLECTION_RAW_REACTIVITIES']
    except KeyError as e:
        msg = "Could not find COLLECTION_RAW_REACTIVITIES in "
        msg += f"the DASHBOARD section of file {user_config_fn}."
        logger.warning(msg)
        sys.exit(0)

    try:
        global COLLECTION_SIG_FN
        COLLECTION_SIG_FN = \
            user_config['DASHBOARD']['COLLECTION_RANGES_DIFF_CONDITIONS']
    except KeyError as e:
        msg = "Could not find COLLECTION_RANGES_DIFF_CONDITIONS in "
        msg += f"the DASHBOARD section of file {user_config_fn}."
        logger.warning(msg)
        sys.exit(0)

    b_debug = ""
    try:
        # beware this is going to be a str
        b_debug = user_config['DEFAULT']['DEBUG']
    except KeyError as e:
        logger.debug("Could not find DEBUG in the DEFAULT section")
        logger.debug(f"of file {user_config_fn}. Using default.")
    if b_debug == "True":
        logger.setLevel(logging.DEBUG)
    elif b_debug == "False":
        logger.setLevel(logging.INFO)


def read_user_config(*, logger, user_config_fn: str):
    """Read in the configuration from the user"""
    user_config = configparser.ConfigParser()
    user_config.read_file(open(user_config_fn))
    user_config.read(user_config_fn)

    try:
        global MONGO_HOST_AND_PORT
        MONGO_HOST_AND_PORT = user_config['DEFAULT']['HOST_AND_PORT']
    except KeyError as e:
        msg = "Could not find HOST_AND_PORT in the DEFAULT section"
        msg += f"of file {user_config_fn}."
        logger.warning(msg)
        sys.exit(0)

    try:
        global MAX_PROCESS_WORKERS
        MAX_PROCESS_WORKERS = \
            int(user_config['DEFAULT']['MAX_PROCESS_WORKERS'])
    except KeyError as e:
        msg = "Could not find MAX_PROCESS_WORKERS in the DEFAULT section"
        msg += f"of file {user_config_fn}."
        logger.warning(msg)
        sys.exit(0)
    if MAX_PROCESS_WORKERS > MAX_ALLOWED_WORKERS:
        msg = f"You specified MAX_WORKERS to be {MAX_PROCESS_WORKERS}, " \
              f"which is very likely too much. Resetting it to " \
              f" {MAX_ALLOWED_WORKERS}."
        logger.warning(msg)
        MAX_PROCESS_WORKERS = MAX_ALLOWED_WORKERS

    b_debug = ""
    try:
        # beware this is going to be a str
        b_debug = user_config['DEFAULT']['DEBUG']
    except KeyError as e:
        logger.debug("Could not find DEBUG in the DEFAULT section")
        logger.debug(f"of file {user_config_fn}. Using default.")
    if b_debug == "True":
        logger.setLevel(logging.DEBUG)
    elif b_debug == "False":
        logger.setLevel(logging.INFO)


##########################################################################
# Logging configuration
# It should read the 'DEBUG' config from above
##########################################################################
if rg4_react['DEBUG']:
    console_level = 'DEBUG'
else:
    console_level = 'INFO'

LOG_SETTINGS = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'level': console_level,
            'formatter': 'simple',
            'stream': 'ext://sys.stdout',
        },
        'file': {
            'class': 'logging.handlers.RotatingFileHandler',
            'level': 'INFO',
            'formatter': 'detailed',
            'filename': '/tmp/junk.log',
            'mode': 'a',
            'maxBytes': 10485760,
            'backupCount': 5,
        },

    },
    'formatters': {
        'detailed': {
            'format': '%(asctime)s %(module)-17s line:%(lineno)-4d ' \
                      '%(levelname)-8s %(message)s',
        },
        'email': {
            'format': 'Timestamp: %(asctime)s\nModule: %(module)s\n' \
                      'Line: %(lineno)d\nMessage: %(message)s',
        },
        'simple': {
            'format': '[%(levelname)-7s] %(module)15s : %(message)s',
        }
    },
    'loggers': {
        'extensive': {
            'level': 'DEBUG',
            'handlers': ['file', ]
        },
        'rg4_logger': {
            'level': console_level,
            'handlers': ['console', ],
            'propagate': False,
        },

        'root': {
            'level': console_level,
            'handlers': ['console']
        },

    },
}

Config = namedtuple('configuration',
                    ['rg4_react', 'LOG_SETTINGS', 'dashboard'])

config = Config(rg4_react, LOG_SETTINGS, dashboard)

logging.config.dictConfig(config.LOG_SETTINGS, )
