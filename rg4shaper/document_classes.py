"""Classes to add documents to mongodb."""

import bisect
from collections import namedtuple
from typing import Union

Range = namedtuple('Range', ['start', 'end'])

VALID_COMP_KEYS = ('K-Li', 'K-PDS', 'Li-PDS')


class DocumentRG4(object):
    """Store experiment attributes and results."""

    comparison_keys = VALID_COMP_KEYS

    def __init__(self, cell_type=None, environment=None, g4_inducer=None):
        """Initialize."""
        self.cell_type = cell_type
        self.environment = environment
        self.g4_inducer = g4_inducer

    def __repr__(self):
        """Represent."""
        return self.__dict__.__str__()


class ConsensusRG4(DocumentRG4):
    """Store consensus results for a given condition."""

    def __init__(self, cell_type=None, environment=None, g4_inducer=None,
                 k_li_res=None, k_pds_res=None, li_pds_res=None):
        """Initialize attributes and attributes of the superclass."""
        super().__init__(cell_type=cell_type, environment=environment,
                         g4_inducer=g4_inducer)
        self.is_consensus = True
        self.range_replicates = {'K-Li': k_li_res,
                                 'K-PDS': k_pds_res,
                                 'Li-PDS': li_pds_res,
                                 }

    def bulk_insert(self, cell_type=None, environment=None, g4_inducer=None,
                    compared_condition=None, result_dict=None):
        """Set more than attribute at a time."""
        if cell_type is not None:
            self.cell_type = cell_type
        if environment is not None:
            self.environment = environment
        if g4_inducer is not None:
            self.g4_inducer = g4_inducer

        if compared_condition is not None:
            if compared_condition in self.comparison_keys:
                self.range_replicates[compared_condition] = result_dict
            else:
                raise ValueError('Wrong comparison key {0}, must be on of {1}'
                                 .format(compared_condition,
                                         self.comparison_keys))

    def aggregate_consensus(self, results_dict=None, compared_condition=None):
        """Count the consensus regions for a given pair of conditions.

        Input
        -----
        : results_dict - dict containing array of array. The key is the
                    comparison condition, and the innermost arrays are Range
        : compared_condition - string with the condition comparison,
                    must be one of ['K-Li', 'K-PDS', 'Li-PDS']

        Output
        ------
        : consensus - dict with transcript_id as key, and values another dict
                    where the keys are the ranges and their occurrences across
                    replicates the ranges are stringified as "start+end",
                    to be able to store them as json
        """
        if compared_condition not in VALID_COMP_KEYS:
            raise ValueError(
                "Wrong pair of conditions: {0:s}".format(compared_condition))

        if self.range_replicates[compared_condition]:
            __consensus = self.range_replicates[compared_condition]
        else:
            __consensus = {}
        for transc_id, intervals in results_dict[compared_condition].items():
            if transc_id not in __consensus:
                __consensus[transc_id] = {}
                for interval in intervals:
                    # stringify the key for storing later in mongodb
                    key = "+".join([str(x) for x in interval])
                    __consensus[transc_id][key] = 1
            else:
                for interval in intervals:
                    # stringify the key for storing later in mongodb
                    key = "+".join([str(x) for x in interval])
                    try:
                        __consensus[transc_id][key] += 1
                    except KeyError:
                        similar_key = find_similar_key(
                            prev_key=__consensus[transc_id],
                            candidate=Range(*interval),
                        )
                        if similar_key:
                            # stringify the key for storing later in mongodb
                            str_similar_key = "+".join([str(x)
                                                        for x in similar_key])
                            __consensus[transc_id][str_similar_key] += 1
                        else:
                            __consensus[transc_id][key] = 1

        self.range_replicates[compared_condition] = __consensus

    def __repr__(self):
        """Represent."""
        return self.__dict__.__str__()


class ReactG4(DocumentRG4):
    """Store reactivities and differences between conditions."""

    def __init__(self, cell_type=None, environment=None, g4_inducer=None,
                 k_fn=None, li_fn=None, pds_fn=None,
                 k_li_res=None, k_pds_res=None, li_pds_res=None):
        """Initialize attributes and attributes of the superclass."""
        super().__init__(
            cell_type=cell_type,
            environment=environment,
            g4_inducer=g4_inducer,
        )

        self.reactivities = {
            'K': k_fn,
            'Li': li_fn,
            'PDS': pds_fn,
        }
        # keys are in lexicographic order, since they are
        # generated in compare conditions by itertools.combine
        self.conditions_diff_ranges = {
            'K-Li': k_li_res,
            'K-PDS': k_pds_res,
            'Li-PDS': li_pds_res,
        }

    def bulk_insert(self, cell_type=None, environment=None, g4_inducer=None,
                    k_fn=None, li_fn=None, pds_fn=None,
                    k_li_res=None, k_pds_res=None, li_pds_res=None):
        """Add a new value for one or more than one instance attribute."""
        if cell_type is not None:
            self.cell_type = cell_type
        if environment is not None:
            self.environment = environment
        if g4_inducer is not None:
            self.g4_inducer = g4_inducer
        if k_fn is not None:
            self.reactivities['K'] = k_fn
        if li_fn is not None:
            self.reactivities['Li'] = li_fn
        if pds_fn is not None:
            self.reactivities['PDS'] = pds_fn

        if k_li_res is not None:
            self.conditions_diff_ranges['K-Li'] = k_li_res
        if k_pds_res is not None:
            self.conditions_diff_ranges['K-PDS'] = k_pds_res
        if li_pds_res is not None:
            self.conditions_diff_ranges['Li-PDS'] = li_pds_res

    def __repr__(self):
        """Represent."""
        return self.__dict__.__str__()


def clean_keys(prev_key=None):
    """Transform str keys to int intervals."""
    clean = []
    for k in prev_key:
        s, e = [int(x) for x in k.split("+")]
        clean.append(Range(s, e))
    return clean


def overlap(range_one: Range, range_two: Range):
    """Return the overlap between ranges one and two.

    Input
    -----
    : range_one, range_two : namedtuple ["start", "end"]
        The namedtuple *must* be properly build for the overlap
        to make sense (e.g end > start)
    Output
    ------
    : int - the overlap
    """
    s = sorted([range_one, range_two], key=lambda x: x.start)
    return s[0].end - s[1].start


def find_similar_key(*, prev_key: list, candidate: namedtuple,
                     min_overlap: int = 1) -> Union[namedtuple, None]:
    """Return a key with at least given overlap to a list of intervals.

    :param min_overlap: minimum length to consider range as similar
    :param candidate: namedtuple Range
    :param prev_key: list of namedtuple Range
    """
    max_overlap = 0
    similar_key = []
    # cut down n -> ~ log(n) the number of comparisons by finding
    # the key neighbours to inspect using bisection on a sorted list
    sorted_prev_keys = sorted(clean_keys(prev_key), key=lambda x: x.start)
    bis = bisect.bisect_left(sorted_prev_keys, candidate)

    # deal with corner cases
    if bis == len(sorted_prev_keys):
        look_for = [sorted_prev_keys[bis - 1]]
    elif bis == 0:
        look_for = [sorted_prev_keys[bis]]
    else:
        look_for = [sorted_prev_keys[bis], sorted_prev_keys[bis - 1]]

    # check the neighbours
    for pk in look_for:
        current_overlap = overlap(candidate, pk)
        # make sure we find only meaningful overlaps
        if current_overlap >= min_overlap:
            if current_overlap > max_overlap:
                similar_key = pk
                max_overlap = current_overlap

    if len(similar_key) > 0:
        return similar_key
    else:
        return None
