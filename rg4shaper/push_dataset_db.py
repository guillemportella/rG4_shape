import logging
from collections import namedtuple
from contextlib import contextmanager

import pandas as pd
from pymongo import MongoClient

from rg4shaper.load_config import config

logger = logging.getLogger("rg4_logger")

MONGO_HOST_AND_PORT = config.rg4_react['MONGO_PORT_AND_HOST']

DbCol = namedtuple('db_and_collection', ['db', 'collection'])


@contextmanager
def getcollection(*, db_n, col_n):
    client = MongoClient(host=[MONGO_HOST_AND_PORT])  # open a connection
    db = client[db_n]
    col = db[col_n]
    yield DbCol(db, col)


def push_vivo_data(*, df_name: str, col):
    """Push the in vivo data"""

    df = pd.read_csv(df_name, sep=" ")

    vivo_list = \
        df[(df['Channel_type'] == 'plus') & (
                df['Folding_environment'] == 'vivo')
           & (df['Cell_type'] == "Hela")][
            ['File_id', 'Ionic_condition', 'Condition_id',
             'Condition_id_count']]

    for idd, v in vivo_list.iterrows():
        if v['Ionic_condition'] == "-":
            q = {'environment': 'vivo',
                 'ionic': 'K',
                 'cell_type': 'Hela'}
            to_upsert = {
                '$push': {'fname': v['File_id']},
                '$inc': {'replicates': 1}
            }
            col.update_one(q, to_upsert, upsert=True)
        elif v['Ionic_condition'] == "cPDS":
            q = {'environment': 'vivo',
                 'ionic': 'KcPDS',
                 'cell_type': 'Hela'}
            to_upsert = {
                '$push': {'fname': v['File_id']},
                '$inc': {'replicates': 1}
            }
            col.update_one(q, to_upsert, upsert=True)
        else:
            q = {'environment': 'vivo',
                 'ionic': v['Ionic_condition'],
                 'cell_type': 'Hela'}
            to_upsert = {
                '$push': {'fname': v['File_id']},
                '$inc': {'replicates': 1}
            }
            col.update_one(q, to_upsert, upsert=True)

    logger.info(f"Done inserting in vivo")


def push_vitro_data(*, df_name: str, col):
    """Push the in vitro data to db"""
    df = pd.read_csv(df_name, sep=" ")

    for cell_type in ['Hela']:
        pd_ct = df[(df['Cell_type'] == cell_type) & (
                df['Folding_environment'] == 'vitro')]

        for m in set(pd_ct['Corresponding_minus'].dropna()):
            to_insert = pd_ct[pd_ct['Corresponding_minus'] == m][
                ['File_id', 'Ionic_condition']]
            for ii, v in to_insert.iterrows():
                q = {'environment': 'vitro',
                     'ionic': v['Ionic_condition'],
                     'cell_type': 'Hela'}
                to_upsert = {
                    '$push': {'fname': v['File_id']},
                    '$inc': {'replicates': 1}
                }
                col.update_one(q, to_upsert, upsert=True)

    logger.info(f"Done inserting in vitro")


def push_dataset_info(args):
    """Push conditions and file names into database"""
    df_name = args['l']
    db_name = args['db']
    col_name = args['cn']
    b_clean_collection = args['clean_collection']

    with getcollection(db_n=db_name, col_n=col_name) as db_info:
        if b_clean_collection:
            db_info.db.drop_collection(db_info.collection)
            logger.info(f"Dropped {col_name}")
        else:
            push_vivo_data(df_name=df_name, col=db_info.collection)
            push_vitro_data(df_name=df_name, col=db_info.collection)

    return 0
