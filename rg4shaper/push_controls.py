"""Add controls to the database."""
import logging
import ntpath

import pandas as pd
from pymongo import MongoClient

from rg4shaper.document_classes import Range
from rg4shaper.load_config import config

logger = logging.getLogger("rg4_logger")

MONGO_HOST_AND_PORT = config.rg4_react['MONGO_PORT_AND_HOST']


def push_regions_to_database(ctrl_fn, collection, ctrl_type,
                             col_react, b_filter):
    """Push the control transcript_id and Range from  the control filename."""
    df = pd.read_csv(ctrl_fn, sep="\t")
    logger.warning("Assuming that your header at least contains:")
    logger.warning(
        "refseq  start_constraint  end_constraint  rts_value p_value class_x")
    logger.warning("and the ranges are *1-based*.")

    to_ins = []
    if b_filter:
        for t, s, e, pval, cl, rts in zip(df.refseq, df.start_constraint - 1,
                                          df.end_constraint - 1, df.p_value,
                                          df.class_x, df.rts_value):
            cross_query = {'transc_id': t}
            if col_react.find_one(cross_query) is not None:
                entry = {'transc_id': t,
                         'range': Range(int(s), int(e)),
                         'control_type': ctrl_type,
                         'p_value': pval,
                         'class': cl,
                         'rts_value': rts,
                         }
                to_ins.append(entry)
    else:
        to_ins = [{'transc_id': t,
                   'range': Range(int(s), int(e)),
                   'control_type': ctrl_type,
                   'p_value': pval,
                   'class': cl,
                   'rts_value': rts,
                   }
                  for t, s, e, pval, cl, rts in
                  zip(df.refseq, df.start_constraint - 1,
                      df.end_constraint - 1,
                      df.p_value, df.class_x, df.rts_value)]

    collection.insert_many(to_ins)


def push_transcript_names(ctrl_fn=None, collection=None, ctrl_type=None):
    """Push the control transcript_id and Range from the control filename."""
    df = pd.read_csv(ctrl_fn, sep="\t")
    to_ins = {"transc_id": list(df.transcript), 'control_type': ctrl_type}
    collection.insert_one(to_ins)


def controls_db(args):
    """Push controls to their database."""
    fname = args['fn']
    db_name = args['db']
    col_name = args['cn']
    filter_cn = args['filter_c']
    if not col_name:
        col_name = ntpath.basename(fname).split(".")[0]
    col_name = "control." + col_name
    b_clean_collection = args['clean_collection']
    b_just_names = args['only_names']
    b_filter = False

    ctrl_type = args['type']

    client = MongoClient(host=[MONGO_HOST_AND_PORT])
    db = client[db_name]
    collection = db[col_name]
    col_react = None
    if filter_cn:
        logger.warning(f"Filtering transcripts using {filter_cn}")
        b_filter = True
        col_react = db[filter_cn]

    if b_clean_collection:
        db.drop_collection(col_name)
        logger.info(f"Dropped {col_name}")
    elif b_just_names:
        push_transcript_names(
            ctrl_fn=fname,
            collection=collection,
            ctrl_type=ctrl_type,
        )
        logger.info(f"Done pushing contents of {fname}")
    else:
        push_regions_to_database(
            ctrl_fn=fname,
            collection=collection,
            ctrl_type=ctrl_type,
            col_react=col_react,
            b_filter=b_filter,
        )
        logger.info(f"Done pushing contents of {fname}")
        return 1
