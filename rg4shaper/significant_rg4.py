import asyncio
import bz2
import gzip
import logging
import os
import pickle
import re
import sys
from collections import namedtuple
from concurrent import futures
from contextlib import contextmanager
from functools import partial
from typing import List, Set, Dict

import motor.motor_asyncio
import pandas as pd
from Bio import SeqIO
from dask.distributed import as_completed, Client
from pymongo import MongoClient
from pymongo.errors import BulkWriteError

import rg4shaper.compare_conditions as ccg4
import rg4shaper.document_classes as order
import rg4shaper.load_config as gconf
from rg4shaper.document_classes import Range
from rg4shaper.load_config import config, read_user_config

logger = logging.getLogger("rg4_logger")

gconf.MONGO_HOST_AND_PORT = config.rg4_react['MONGO_PORT_AND_HOST']

gconf.MAX_PROCESS_WORKERS = int(config.rg4_react['MAX_PROCESS_WORKERS'])

# COMPARE_METRICS = ['rmsd', 'gini']
COMPARE_METRICS = ['rmsd', 'integration']

DbCol = namedtuple('db_and_collection', ['db', 'collection'])

# Warning: in order for multiprocessing to pickle this, the name of the type
# and the typename must match, or else pickle can not find the reference
# to this class. dill would work, but mp does not use it.
Reference_conditions = namedtuple("Reference_conditions",
                                  ['ionic', 'environment'])

METRICS_DICT = {'rmsd': ccg4.rmsd, 'gini': ccg4.gini_diff,
                'integration': ccg4.integration_diff}

REFERENCES_LIST = [Reference_conditions('Li', 'vitro'),
                   Reference_conditions('K', 'vitro'),
                   Reference_conditions('K', 'vivo')]
INDUCERS_LIST = [Reference_conditions('K', 'vivo'),
                 Reference_conditions('KPDS', 'vivo'),
                 Reference_conditions('KcPDS', 'vivo'),
                 Reference_conditions('KPhenDC3', 'vivo'), ]

if os.environ.get('RG4_TESTING', False):
    logger.info(f"Running in TEST mode, fewer comparisons.")
    testing_ref = [Reference_conditions('Li', 'vitro')]
    testing_ind = [Reference_conditions('K', 'vitro')]
    REFERENCES_LIST = testing_ref
    INDUCERS_LIST = testing_ind


@contextmanager
def getcollection(*, db_n, col_n):
    client = MongoClient(host=[gconf.MONGO_HOST_AND_PORT])  # open a connection
    db = client[db_n]
    col = db[col_n]
    yield DbCol(db, col)


class VitrotoDB(object):
    """Find the in vitro data sets and store them in MongoDB

    Each document in the collection has this records,
        {
         'environment': 'vivo',
         'ionic': 'K' or 'KPDS", etc
         'file_name': the file name containing the reactivities,
         'cell_type': 'Hela',
        }
    """

    def __init__(self, *, db_name: str, df_fname: str,
                 store_col_name: str) -> None:
        """Initialize instance.

        :param db_name: name of the database
        :param df_fname: name of dataframe containing list of experiments
        :store_col_name: name of collection to store the info records
        """
        self.db_name = db_name
        self.df_fname = df_fname
        self.store_col_name = store_col_name
        self.dict_to_store = {}
        self.collection = None

    def __enter__(self) -> 'VitrotoDB':
        """Find what to store and open collection."""
        client = MongoClient(
            host=[gconf.MONGO_HOST_AND_PORT])  # open a connection
        db = client[self.db_name]
        self.collection = db[self.store_col_name]
        return self

    def csv_to_dict(self):
        """Store dict of files/conditions for the experiments"""
        d = pd.read_csv(self.df_fname, sep=" ")
        my_df = \
            d[(d['Channel_type'] == 'plus')
              & (d['Cell_type'] == "Hela")][
                ['File_id', 'Ionic_condition', 'Condition_id',
                 'Condition_id_count', 'Folding_environment']]
        list_json = []
        for idd, v in my_df.iterrows():
            if v['Ionic_condition'] == "-":
                dd = {'environment': v['Folding_environment'],
                      'ionic': 'K',
                      'file_name': v['File_id'],
                      'cell_type': 'Hela'
                      }
                list_json.append(dd)
            elif v['Ionic_condition'] == "cPDS":
                dd = {'environment': v['Folding_environment'],
                      'ionic': 'KcPDS',
                      'file_name': v['File_id'],
                      'cell_type': 'Hela'
                      }
                list_json.append(dd)
            else:
                dd = {'environment': v['Folding_environment'],
                      'ionic': v['Ionic_condition'],
                      'file_name': v['File_id'],
                      'cell_type': 'Hela'
                      }
                list_json.append(dd)

        self.dict_to_store = list_json

    def __exit__(self, exc_type, exc_val, exc_tb):
        if len(self.dict_to_store) > 0:
            self.collection.insert_many(self.dict_to_store)
        else:
            logger.error("Nothing to store. Did you run csv_to_dict?")
            sys.exit(1)


def extract_significant(*, seqs, data_inducer, data_ref, regex_g4, regex_g2,
                        slop_g4: int, len_frag, min_pval, metric,
                        reference: Reference_conditions,
                        inducer: Reference_conditions, ref_fname,
                        inducer_fname, cntrl_col, db_name):
    """Find g4/non-g4 regions, compare reactivities and filter by pval.

    :param slop_g4: extend the quadruplex this amount both sides
    :param metric: the name of the metric used to compare two G4 regions
    :param db_name: the name of the database
    :param cntrl_col: a list of control collections
    :param seqs: dict of SeqRecords, where the key is the transcript name
    :param data_inducer: np.array with reactivities of inducer
    :param data_ref: np.array with reactivities of inducer
    :param regex_g4:  regular expression for finding G4s
    :param regex_g2: regular expression for finding something like a G4
                    but much looser in definition. Used to expand the
                    G4 regions when looking for non-G4 regions
    :param len_frag: length of the window sliding across nonG4 intervals
    :param min_pval: minimum p-value to keep a rG4 region as detected
    :param inducer: which G4 inducer was used
    :param reference:  which G4 reference was used
    :param ref_fname: file name containing the reactivities for the reference
    :param inducer_fname: file name containing the reactivities for the
                        inducer
    """

    try:
        use_metric = METRICS_DICT[metric]
    except KeyError as _:
        logger.warning(f"Metric {metric} not available, using RMSD")
        use_metric = METRICS_DICT['rmds']

    g4_tr = \
        ccg4.gx_location_in_transcripts_populated(seqs,
                                                  regex=regex_g4,
                                                  data=[
                                                      data_inducer,
                                                      data_ref, ],
                                                  )
    g4_tr = \
        ccg4.gx_insert_validated_regions(g4_tr,
                                         slop_g4=slop_g4,
                                         data=[data_inducer, data_ref, ],
                                         cntrl_col=cntrl_col,
                                         db_name=db_name,
                                         cntrl2check='PDS')
    g2_tr = \
        ccg4.gx_location_in_transcripts_populated(seqs,
                                                  regex=regex_g2,
                                                  data=[
                                                      data_inducer,
                                                      data_ref, ],
                                                  )
    nong4_tr = ccg4.nong4_in_transcripts(seqs, g4_tr, g2_tr,
                                         data=[data_inducer,
                                               data_ref, ],
                                         len_frag=len_frag,
                                         )
    # The order ref/inducer in compare_xxx is important
    # if using the integration metric in which the integral
    # of the first is removed from the second one.
    # Since we assume that the inducer reduces the amount of shape signal
    # the difference in integration is positive in the rg4s.
    # As we calculate the p-values wrt background as the fraction
    # of times the metric is larger than the background, we want the rg4-like
    # metric to be larger than the background.
    m_non_g4 = ccg4.compare_nong4(data_ref, data_inducer,
                                  nong4_tr,
                                  len_frag=len_frag,
                                  metric=use_metric,
                                  )
    m_g4 = ccg4.compare_g4(data_ref, data_inducer, g4_tr,
                           metric=use_metric,
                           )

    signif = ccg4.filter_significance(w_g4=m_g4,
                                      wo_g4=m_non_g4,
                                      g4_tr=g4_tr,
                                      pval_threshold=min_pval,
                                      )

    # one record would be way too large, we have to split them
    records = []
    for tr, sig in signif.items():
        record_comp = {"is_comparison": True,
                       "reference_fname": ref_fname,
                       "reference_ionic": reference.ionic,
                       "reference_environment": reference.environment,
                       "inducer_fname": inducer_fname,
                       "inducer_ionic": inducer.ionic,
                       "inducer_environment": inducer.environment,
                       'transc_id': tr,
                       'ranges': [[s.start, s.end] for s in sig],
                       'pvalues': [s.pvalue for s in sig],
                       "metric": metric,
                       }
        records.append(record_comp)

    return records


def generate_significance_multiprocess(*, db_name: str, react_col_name: str,
                                       col_name: str,
                                       ctrl_basename: str, seqs_col_name: str,
                                       slop_g4: int, len_frag: int):
    """Ranges with differences in reactivities between inducer and conditions.

    Populates the mongodb database with the ranges of significant differences
    between pairs of inducer-controls.

    Input
    -----
    :param ctrl_basename: the basename of the control database
    :param db_name: name of the database
    :param react_col_name: name of collection holding the reactivities
    :param col_name: name of collection holding info on significant ranges
    :param seqs_col_name: name of the collection holding the sequences
    :param slop_g4: regex for G4
    :param len_frag: length of the window sliding across nonG4 intervals

    """
    capture_around = f"(.{{,{slop_g4}}})"
    rg4_base = '([gG]{3,}\w{1,7}){3,}[gG]{3,}'
    g4 = capture_around + rg4_base + capture_around
    g2 = '([gG]{2,}\w{1,12}){3,}[gG]{2,}'
    regex_g4 = re.compile(g4)
    regex_g2 = re.compile(g2)

    client = MongoClient(host=[gconf.MONGO_HOST_AND_PORT])  # open a connection
    db = client[db_name]
    col_results = db[col_name]
    col_exp = db[react_col_name]
    col_seqs = db[seqs_col_name]
    col_ctrl_pds = [collection for collection in
                    db.collection_names()
                    if ctrl_basename in collection and 'PDS' in collection]

    min_pval = 0.05
    for metric in COMPARE_METRICS:
        # iter around all references

        for ref_cond in REFERENCES_LIST:

            cursor_ref = col_results.find(
                {'ionic': ref_cond.ionic,
                 'environment': ref_cond.environment})

            with futures.ProcessPoolExecutor(
                    max_workers=gconf.MAX_PROCESS_WORKERS) \
                    as executor:
                to_do = []
                # iter for each file containing the reference
                for ref_iter in cursor_ref:

                    cursor_exp = col_exp.find(
                        {'fname': ref_iter['file_name']})

                    data_ref = {
                        cc['transc_id']: pickle.loads(cc['reactivities'])
                        for
                        cc in cursor_exp}

                    # iter around all inducers

                    for inducer_cond in INDUCERS_LIST:

                        cursor_inducer = col_results.find(
                            {'ionic': inducer_cond.ionic,
                             'environment': inducer_cond.environment})

                        # we don't want to compare with itself,
                        # happens for K-vivo
                        if inducer_cond.ionic != ref_cond.ionic or \
                                inducer_cond.environment != \
                                ref_cond.environment:

                            # iter for each file containing the inducer
                            for ind_iter in cursor_inducer:
                                # get experimental data for each transcript
                                cursor_exp = col_exp.find(
                                    {'fname': ind_iter['file_name']})
                                data_inducer = {
                                    cc['transc_id']:
                                        pickle.loads(cc['reactivities'])
                                    for cc in cursor_exp}

                                # we use the transc_ids from data_inducer,
                                # data_ref would have had the same keys
                                # as we pre-filtered
                                # the datasets to have the same transcripts
                                # across conditions
                                transc_ids = list(data_inducer.keys())
                                seqs = \
                                    ccg4.read_selected_seqs_db(col_seqs,
                                                               transc_ids)

                                kwords = {'seqs': seqs,
                                          'data_inducer': data_inducer,
                                          'data_ref': data_ref,
                                          'regex_g4': regex_g4,
                                          'regex_g2': regex_g2,
                                          'slop_g4': slop_g4,
                                          'len_frag': len_frag,
                                          'min_pval': min_pval,
                                          'metric': metric,
                                          'ref_fname': ref_iter['file_name'],
                                          'inducer_fname': ind_iter[
                                              'file_name'],
                                          'reference': ref_cond,
                                          'inducer': inducer_cond,
                                          'cntrl_col': col_ctrl_pds,
                                          'db_name': db_name,
                                          }

                                # submit process and store the future
                                future = executor.submit(
                                    partial(extract_significant, **kwords))
                                to_do.append(future)
                                msg = f"Sub {ref_iter['file_name']} - "
                                msg += f"{ind_iter['file_name']} : "
                                msg += f"Metric: {metric}"
                                logger.info(msg)

                # collect the results from futures
                # and store them all in DB
                for future in futures.as_completed(to_do):
                    res = future.result()
                    ins_records = len(
                        col_results.insert_many(res).inserted_ids)
                    logger.info(f"Inserted {ins_records } records")


def generate_significance(*, db_name: str, react_col_name: str, col_name: str,
                          ctrl_basename: str, seqs_col_name: str,
                          slop_g4: int, len_frag: int, dask_sch: str):
    """Ranges with differences in reactivities between inducer and conditions.

    Populates the mongodb database with the ranges of significant differences
    between pairs of inducer-controls.

    Input
    -----
    :param dask_sch: the path to the dask scheduler
    :param ctrl_basename: the basename of the control database
    :param db_name: name of the database
    :param react_col_name: name of collection holding the reactivities
    :param col_name: name of collection holding info on significant ranges
    :param seqs_col_name: name of the collection holding the sequences
    :param slop_g4: regex for G4
    :param len_frag: length of the window sliding across nonG4 intervals

    """
    capture_around = f"(.{{,{slop_g4}}})"
    rg4_base = '([gG]{3,}\w{1,7}){3,}[gG]{3,}'
    g4 = capture_around + rg4_base + capture_around
    g2 = '([gG]{2,}\w{1,12}){3,}[gG]{2,}'
    regex_g4 = re.compile(g4)
    regex_g2 = re.compile(g2)

    client = MongoClient(host=[gconf.MONGO_HOST_AND_PORT],
                         maxPoolSize=None)  # open a connection
    db = client[db_name]
    col_results = db[col_name]
    col_exp = db[react_col_name]
    col_seqs = db[seqs_col_name]
    col_ctrl_pds = [collection for collection in
                    db.collection_names()
                    if ctrl_basename in collection and 'PDS' in collection]

    dask_client = Client(scheduler_file=dask_sch)  # start client
    logger.info(f"Client is {dask_client}")

    min_pval = 0.05

    to_do = []
    # iter around all references
    for metric in COMPARE_METRICS:
        for ref_cond in REFERENCES_LIST:

            cursor_ref = col_results.find(
                {'ionic': ref_cond.ionic,
                 'environment': ref_cond.environment})

            # iter for each file containing the reference
            for ref_iter in cursor_ref:

                cursor_exp = col_exp.find(
                    {'fname': ref_iter['file_name']})

                data_ref = {cc['transc_id']: pickle.loads(cc['reactivities'])
                            for
                            cc in cursor_exp}

                # iter around all inducers
                for inducer_cond in INDUCERS_LIST:

                    cursor_inducer = col_results.find(
                        {'ionic': inducer_cond.ionic,
                         'environment': inducer_cond.environment})

                    # we don't want to compare with itself, happens for K-vivo
                    if inducer_cond.ionic != ref_cond.ionic or \
                            inducer_cond.environment != ref_cond.environment:

                        # iter for each file containing the inducer
                        for ind_iter in cursor_inducer:
                            # get experimental data for each transcript
                            cursor_exp = col_exp.find(
                                {'fname': ind_iter['file_name']})
                            data_inducer = {
                                cc['transc_id']: pickle.loads(
                                    cc['reactivities'])
                                for cc in cursor_exp}

                            # we use the transc_ids from data_inducer, data_ref
                            # would have had the same keys as we pre-filtered
                            # the datasets to have the same transcripts
                            # across conditions
                            seqs = ccg4.read_selected_seqs_db(col_seqs, list(
                                data_inducer.keys()))

                            kwords = {'seqs': seqs,
                                      'data_inducer': data_inducer,
                                      'data_ref': data_ref,
                                      'regex_g4': regex_g4,
                                      'regex_g2': regex_g2,
                                      'slop_g4': slop_g4,
                                      'len_frag': len_frag,
                                      'min_pval': min_pval,
                                      'metric': metric,
                                      'ref_fname': ref_iter['file_name'],
                                      'inducer_fname': ind_iter['file_name'],
                                      'reference': ref_cond,
                                      'inducer': inducer_cond,
                                      'cntrl_col': col_ctrl_pds,
                                      'db_name': db_name,
                                      }
                            d_futures = dask_client.submit(extract_significant,
                                                           **kwords)
                            to_do.append(d_futures)
                            msg = f"Sub {ind_iter['file_name']} - "
                            msg += f"{ind_iter['file_name']} : "
                            msg += f"Metric: {metric}"
                            logger.info(msg)

    # collect the results from futures and store
    for future, result in as_completed(to_do, with_results=True):
        try:
            ins_rec = len(col_results.insert_many(result).inserted_ids)
        except BulkWriteError as bwe:
            logger.error(bwe.details)
            # you can also take this component and do more analysis
            werrors = bwe.details['writeErrors']
            logger.error(werrors)
            logger.error(f"Result is {result}")
            raise
        logger.info(f"Inserted {ins_rec } records")

    dask_client.close()


def get_unique_transc_names(inp_fasta) -> Set:
    """Return the all (unique) name ids in seq_fn


    Input
    -----
    :param inp_fasta: fasta file, can be gz or bz2 compressed

    Ouput
    -----
    records - set of unique records
    """
    if inp_fasta.split(".")[-1] == "bz2":
        handle = bz2.BZ2File(inp_fasta, "r")
    elif inp_fasta.split(".")[-1] == "gz":
        handle = gzip.open(inp_fasta, "rt")
    else:
        handle = open(inp_fasta, "r")
    unique_transc_id = set()
    for seq_record in SeqIO.parse(handle, 'fasta'):
        unique_transc_id.update([seq_record.id])
    return unique_transc_id


def aggregate_in_memory(*, reference: Reference_conditions,
                        inducer: Reference_conditions,
                        metric: str,
                        records_per_inducer: int, col_name: str,
                        db_name: str) -> Dict:
    """Query DB and aggregate in memory.

    This is meant to run either sequentially, called from multiprocessing
    or from a dask distributed job.

    Concurrent access/edit to the DB is a bottleneck. The idea is to
    off-load the work done in database as much as possible.
    """

    # let's try an extreme case
    client_motor = \
        motor.motor_asyncio.AsyncIOMotorClient(gconf.MONGO_HOST_AND_PORT)
    db = client_motor[db_name]
    col_results = db[col_name]

    mega_d = {}

    async def big_query():
        query = {'is_comparison': True,
                 'reference_ionic': reference.ionic,
                 'reference_environment': reference.environment,
                 'inducer_ionic': inducer.ionic,
                 'inducer_environment': inducer.environment,
                 'metric': metric,
                 }
        cursor = col_results.find(query)
        for cur in await cursor.to_list(length=1000):
            transc_id = cur['transc_id']
            for rg, pval in zip(cur['ranges'], cur['pvalues']):
                key_d = "_".join([transc_id, str(rg[0]), str(rg[1])])
                if key_d not in mega_d:
                    entry = {'transc_id': transc_id,
                             'range': rg,
                             'records_inducer': records_per_inducer,
                             'reference_ionic': reference.ionic,
                             'reference_environment': reference.environment,
                             'inducer_ionic': inducer.ionic,
                             'inducer_environment': inducer.environment,
                             'metric': metric,
                             'validated': False,
                             'score': 1,
                             'react_pairs': [
                                 [cur['reference_fname'], cur['inducer_fname']]
                             ],
                             'pvalues': [pval],
                             }

                    mega_d[key_d] = entry
                else:
                    mega_d[key_d]['score'] += 1
                    mega_d[key_d]['react_pairs'].append(
                        [cur['reference_fname'],
                         cur['inducer_fname']])
                    mega_d[key_d]['pvalues'].append(pval)

    loop = asyncio.get_event_loop()
    _ = loop.run_until_complete(big_query())

    # You might want to consider returning an iterator over the dict
    # values if dict becomes too large.
    return mega_d


async def aggregate_in_memory_async(*, reference: Reference_conditions,
                                    inducer: Reference_conditions,
                                    metric: str,
                                    records_per_inducer: int, col_name: str,
                                    db_name: str) -> Dict:
    """Query DB and aggregate in memory.
    This is meant to be called from asyncio loop calls


    Concurrent access/edit to the DB is a bottleneck. The idea is to
    off-load the work done in database as much as possible.
    """

    # let's try an extreme case
    client_motor = \
        motor.motor_asyncio.AsyncIOMotorClient(gconf.MONGO_HOST_AND_PORT)
    db = client_motor[db_name]
    col_results = db[col_name]

    mega_d = {}

    # async def big_query():
    query = {'is_comparison': True,
             'reference_ionic': reference.ionic,
             'reference_environment': reference.environment,
             'inducer_ionic': inducer.ionic,
             'inducer_environment': inducer.environment,
             'metric': metric,
             }
    cursor = col_results.find(query)
    for cur in await cursor.to_list(length=1000):
        transc_id = cur['transc_id']
        for rg, pval in zip(cur['ranges'], cur['pvalues']):
            key_d = "_".join([transc_id, str(rg[0]), str(rg[1])])
            if key_d not in mega_d:
                entry = {'transc_id': transc_id,
                         'range': rg,
                         'records_inducer': records_per_inducer,
                         'reference_ionic': reference.ionic,
                         'reference_environment': reference.environment,
                         'inducer_ionic': inducer.ionic,
                         'inducer_environment': inducer.environment,
                         'metric': metric,
                         'validated': False,
                         'score': 1,
                         'react_pairs': [
                             [cur['reference_fname'], cur['inducer_fname']]
                         ],
                         'pvalues': [pval],
                         }

                mega_d[key_d] = entry
            else:
                mega_d[key_d]['score'] += 1
                mega_d[key_d]['react_pairs'].append([cur['reference_fname'],
                                                     cur['inducer_fname']])
                mega_d[key_d]['pvalues'].append(pval)

    return mega_d


def aggregate_by_transcript_dask(*, db_name: str, col_name: str,
                                 dask_sch: str):
    """Extract transcript statistics.

    For each inducer, and for each transcript in the inducer, finds
    _the number of replicates for a given range. Also stores the number
    of replicates for that particular transcript and condition.

    Input
    -----
    :param dask_sch: path to the dask scheduler json
    :param col_name: the name of collection keeping the ranges
    :param db_name: the name of the mongodb
    """
    client = MongoClient(host=[gconf.MONGO_HOST_AND_PORT],
                         maxPoolSize=None)  # open a connection
    db = client[db_name]
    col_results = db[col_name]

    dask_client = Client(scheduler_file=dask_sch)  # start client
    logger.info(f"Client is {dask_client}")

    to_do = []
    for reference in REFERENCES_LIST:
        for inducer in INDUCERS_LIST:

            if inducer.ionic != reference.ionic or \
                    inducer.environment != reference.environment:

                for metric in COMPARE_METRICS:
                    distinct_cursor = col_results.aggregate([
                        {'$match': {'is_comparison': True,
                                    'reference_ionic': reference.ionic,
                                    'reference_environment':
                                        reference.environment,
                                    'inducer_ionic': inducer.ionic,
                                    'inducer_environment':
                                        inducer.environment,
                                    'metric': metric,
                                    }
                         },
                        {"$group": {"_id":
                            {
                                'reference_fname': "$reference_fname",
                                'inducer_fname': "$inducer_fname"
                            }
                        }
                        }
                    ])

                    records_inducer = sum(1 for _ in distinct_cursor)
                    msg = f"There are {records_inducer} records/inducer "
                    msg += f"for {inducer} in metric {metric}"
                    logger.info(msg)

                    kwords = {'reference': reference,
                              'inducer': inducer,
                              'metric': metric,
                              'col_name': col_name,
                              'db_name': db_name,
                              'records_per_inducer': records_inducer,
                              }

                    msg = f"Doing {reference.ionic}-{reference.environment} "
                    msg += f"vs {inducer.ionic}-{inducer.environment} "
                    msg += "aggregation"
                    logger.info(msg)

                    my_futures = dask_client.submit(
                        aggregate_in_memory, **kwords)
                    to_do.append(my_futures)
                    msg = f"Done submitting one batch, moving on..."
                    logger.debug(msg)

    for future, result in as_completed(to_do, with_results=True):
        try:
            ins_rec = len(col_results.insert_many(result).inserted_ids)
        except BulkWriteError as bwe:
            logger.error(bwe.details)
            werrors = bwe.details['writeErrors']
            logger.error(werrors)
            logger.error(f"Result is {result}")
            raise
        logger.info(f"Inserted {ins_rec } records")

    dask_client.close()


def aggregate_by_transcript_mp(*, db_name: str, col_name: str):
    """Extract transcript statistics.

    For each inducer, and for each transcript in the inducer, finds
    _the number of replicates for a given range. Also stores the number
    of replicates for that particular transcript and condition.

    Input
    -----
    :param col_name: the name of collection keeping the ranges
    :param db_name: the name of the mongodb
    """
    client = MongoClient(gconf.MONGO_HOST_AND_PORT)  # open a connection
    db = client[db_name]
    col_results = db[col_name]

    for metric in COMPARE_METRICS:
        for reference in REFERENCES_LIST:
            to_do = []
            with futures.ProcessPoolExecutor(
                    max_workers=gconf.MAX_PROCESS_WORKERS) \
                    as executor:
                for inducer in INDUCERS_LIST:

                    if inducer.ionic != reference.ionic or \
                            inducer.environment != reference.environment:
                        distinct_cursor = col_results.aggregate([
                            {'$match': {'is_comparison': True,
                                        'reference_ionic': reference.ionic,
                                        'reference_environment':
                                            reference.environment,
                                        'inducer_ionic': inducer.ionic,
                                        'inducer_environment':
                                            inducer.environment,
                                        'metric': metric,
                                        }
                             },
                            {"$group": {"_id":
                                {
                                    'reference_fname': "$reference_fname",
                                    'inducer_fname': "$inducer_fname"}
                            }
                            }
                        ])

                        records_inducer = sum(1 for _ in distinct_cursor)
                        msg = f"There are {records_inducer} records "
                        msg += f"for {reference} - {inducer} and {metric}"
                        logger.info(msg)

                        kwords = {'reference': reference,
                                  'inducer': inducer,
                                  'metric': metric,
                                  'records_per_inducer':
                                      records_inducer,
                                  'col_name': col_name,
                                  'db_name': db_name}

                        job = executor.submit(partial(aggregate_in_memory,
                                                      **kwords))
                        to_do.append(job)
                        msg = f"Doing {reference.ionic}-"
                        msg += f"{reference.environment} "
                        msg += f"vs {inducer.ionic}-{inducer.environment} "
                        msg += "aggregation"
                        logger.info(msg)

                for future in futures.as_completed(to_do):
                    agg_dict = future.result()
                    if agg_dict:
                        to_ins = [_ for _ in agg_dict.values()]
                        ins_records = len(
                            col_results.insert_many(to_ins).inserted_ids)
                        msg = f"Done inserting {ins_records} for this batch."
                        logger.debug(msg)
                    else:
                        msg = f"Woops, empty batch."
                        logger.debug(msg)


def do_tp_fp(*, list_cntrl_col: List[str], col_signif,
             cntrl2check: str, db, min_overlap=-30):
    """ Update  collection to reflect presence/absence in controls.

    Input
    -----

    :param min_overlap: allowed gap for range merging, lower won't be merged
    :param list_cntrl_col: list of control collection names
    :param col_signif: the collection where the rG4 ranges are, from the exp.
    :param cntrl2check: which control type to check, e.g. PDS
    :param db: the client database, from client[db_name]
    Output
    ------
    Does not return, updates col_signif in the db
    """

    # fill up list of dicts containing the positive controls
    # the idea is to look for one cntrlcheck at the time, but we could
    # have replicates for the same type, hence the list
    col_cntrls_positive = [col for col in list_cntrl_col
                           if "positive" in col and cntrl2check in col]
    query_positive = {'control_type': {'$regex': cntrl2check}}
    list_pos_ranges = []
    for col in col_cntrls_positive:
        pos_ranges = {}
        p_cursor = db[col].find(query_positive)
        for p in p_cursor:
            pos_ranges.setdefault(p['transc_id'], []).append(
                Range(*p['range']))
        list_pos_ranges.append(pos_ranges)

    query_signif = {'validated': {'$exists': True}, }

    matched_exp = col_signif.find(query_signif)
    if matched_exp.count() == 0:
        logger.warning(
            f'Warning, querying {query_signif} did not return any match')
        return
    for m in matched_exp:
        for ref_ranges in list_pos_ranges:
            if m['transc_id'] in ref_ranges:
                for rgs in ref_ranges[m['transc_id']]:
                    ov = order.overlap(Range(*m['range']), rgs)
                    if ov > min_overlap:
                        col_signif.update_one(
                            {"_id": m["_id"]},
                            {
                                '$set': {'validated': True},
                                '$push': {'in_control': cntrl2check},
                            },
                            upsert=False
                        )
    logger.info("Done finding true positives.")


def validate_transcripts(*, db_name: str, col_name: str, cntrl_basename: str):
    """Check if the transcript/range has been seen in rG4-seq.

    :param db_name: name of the database
    :param col_name: name of the collection holding the transcripts info
    :param cntrl_basename: name of the collection holding the controls

    Creates a list of control collections based on the base name of the
    control collections. These control collection should contain the
    transcript id and the ranges of the detected rG4s in rG4seq.

    Then we go one by one in our list of detected rG4s and check if a
    given transcript has be found or not in the controls. If it has, it
    sets the record 'validated' to true, and adds the type of control
    condition that matched that transcript/range.

    """
    client = MongoClient(gconf.MONGO_HOST_AND_PORT)  # open a connection
    db = client[db_name]
    col_results = db[col_name]
    col_ctrl_basename = cntrl_basename
    col_ctrl_names = [collection for collection in
                      db.collection_names()
                      if col_ctrl_basename in collection]
    if len(col_ctrl_names) == 0:
        # TODO change to throw exception
        logger.critical('Could not find a control collection with ', end='')
        logger.critical(f'basename {col_ctrl_basename} '
                        f'in database {db_name}')
        sys.exit()

    do_tp_fp(list_cntrl_col=col_ctrl_names,
             col_signif=col_results,
             cntrl2check='PDS',
             db=db, )


def clean_up_comparisons(db_name: str, col_name: str):
    """Clean up the 'is_comparison' documents

    They are only really used for the aggregation of the results,
    and they make the database very large.

    """
    client = MongoClient(gconf.MONGO_HOST_AND_PORT)

    query = {'is_comparison': True}

    db = client[db_name]
    col = db[col_name]
    n = col.find(query).count()
    logger.debug(f'{n} documents before deleting docs containing {query}')
    _ = col.delete_many(query)
    n = col.find(query).count()
    logger.debug(f'{n} docs after deletion')


def significant_rg4(arguments):
    """Extract significant rG4 sites with differences in reactivity.

    :param arguments: dictionary of arguments from commandline via argparse

    It can push records to the MongoDB describing the conditions and
    the file name associated to it.

    It will populate the database with the results, annotating the
    inducer as well as the names of the controls and and the inducer.

    """
    list_experiments_fn = arguments['l']
    db_name = arguments['db']
    col_name = arguments['cn']
    react_col_name = arguments['exp_cn']
    col_seqs_fn = arguments['seq_cn']
    len_frag = arguments['lfrag']
    slop_g4 = arguments['slop_g4']
    b_push_db = arguments['push_db']
    b_dask = arguments['dask']
    dask_scheduler = arguments['dask_scheduler']
    b_skip_generate = arguments['skip_gen']
    cntrl_basename = arguments['cc']

    if arguments['cf'] is not None:
        read_user_config(logger=logger, user_config_fn=arguments['cf'][0])

    if b_dask:
        logger.info(f"Using Dask for distributed execution.")
        logger.info(f"Scheduler file is {dask_scheduler}")

    # put the data into DB
    if b_push_db:
        logger.info("Going to push to DB")
        with VitrotoDB(db_name=db_name,
                       df_fname=list_experiments_fn,
                       store_col_name=col_name,
                       ) as v2db:
            v2db.csv_to_dict()
        logger.info("Done pushing to DB")

    # Regular expression for G4s and G2s
    if not b_skip_generate:
        logger.info(f"Generating significant regions...")
        if b_dask:
            generate_significance(db_name=db_name,
                                  react_col_name=react_col_name,
                                  col_name=col_name,
                                  ctrl_basename=cntrl_basename,
                                  seqs_col_name=col_seqs_fn,
                                  slop_g4=slop_g4,
                                  len_frag=len_frag,
                                  dask_sch=dask_scheduler,
                                  )
            logger.info(f"Done finding significant ranges.")
            logger.info(f"Aggregating results from replicates... ")
            aggregate_by_transcript_dask(db_name=db_name,
                                         col_name=col_name,
                                         dask_sch=dask_scheduler,
                                         )
            logger.info(f"Done aggregating results from replicates.")
            logger.info("Cleaning up intermediate documents.")
            clean_up_comparisons(db_name=db_name,
                                 col_name=col_name)
            logger.info("Done cleaning up intermediate documents.")
            logger.info(f"Validating results against the controls...")
            validate_transcripts(db_name=db_name, col_name=col_name,
                                 cntrl_basename=cntrl_basename)
            logger.info(f"Done validating segments.")
        else:
            logger.info(f"Using {gconf.MAX_PROCESS_WORKERS} processes")
            generate_significance_multiprocess(db_name=db_name,
                                               react_col_name=react_col_name,
                                               col_name=col_name,
                                               ctrl_basename=cntrl_basename,
                                               seqs_col_name=col_seqs_fn,
                                               slop_g4=slop_g4,
                                               len_frag=len_frag,
                                               )
            logger.info(f"Done finding significant ranges.")
            logger.info(f"Aggregating results from replicates... ")
            aggregate_by_transcript_mp(db_name=db_name,
                                       col_name=col_name,
                                       )
            logger.info(f"Done aggregating results from replicates.")
            logger.info("Cleaning up intermediate documents.")
            clean_up_comparisons(db_name=db_name,
                                 col_name=col_name)
            logger.info("Done cleaning up intermediate documents.")
            logger.info(f"Validating results against the controls...")
            validate_transcripts(db_name=db_name, col_name=col_name,
                                 cntrl_basename=cntrl_basename)
            logger.info(f"Done validating segments.")

    logger.info("All done, have nice day.")
