"""Tools to report regions of dissimilar reactivities across conditions.

The main function is compute_significant_g4_based()
"""
import bz2
import gzip
import itertools
import pickle
import warnings
from collections import namedtuple
from contextlib import contextmanager
from typing import Pattern, List, Dict

import attr
import numpy as np
from Bio import SeqIO
from pymongo import MongoClient

import rg4shaper.load_config as gconf
from rg4shaper.document_classes import Range
from rg4shaper.load_config import config

gconf.MONGO_HOST_AND_PORT = config.rg4_react['MONGO_PORT_AND_HOST']

DbCol = namedtuple('db_and_collection', ['db', 'collection'])

import logging

logger = logging.getLogger('rg4_logger')


@attr.s
class SignificantResult(object):
    """Store panel info"""
    start: int = attr.ib()
    end: int = attr.ib()
    pvalue: int = attr.ib()
    metric: float = attr.ib()
    bg_metrics: List[float] = attr.ib(default=attr.Factory(list))


@contextmanager
def getcollection(*, db_n, col_n):
    client = MongoClient(host=[gconf.MONGO_HOST_AND_PORT],
                         maxPoolSize=None)  # open a connection
    db = client[db_n]
    col = db[col_n]
    yield DbCol(db, col)


def rmsd(one, two):
    """Return the RMSD between one and two."""
    if len(one) != len(two) or len(one) == 0:
        return None
    else:
        return np.sqrt(((one - two) ** 2).mean())


def gini(array):
    """Calculate the Gini coefficient of a numpy array.

    Input
    -----
    : array - a 1d numpy array
    Output
    ------
    : float - the Gini coefficient of array
    """
    # based on bottom eq in
    #  http://www.statsdirect.com/help/content/image/stat0206_wmf.gif
    # from
    # http://www.statsdirect.com/help/default.htm#nonparametric_methods/gini.htm

    array = array.flatten()  # all values treated equally, arrays must be 1d
    if np.amin(array) < 0:
        array -= np.amin(array)  # values cannot be negative
    array += 0.0000001  # values cannot be 0
    array = np.sort(array)  # values must be sorted
    index = np.arange(1, array.shape[0] + 1)  # index per array element
    n = array.shape[0]  # number of array elements
    # Gini coefficient
    return (np.sum((2 * index - n - 1) * array)) / (n * np.sum(array))


def integration_diff(one: np.array, two: np.array):
    """Return the differences of the integral between one and two.

    *The result is signed, depends on the order*

    **We do not check by zero length arrays**

    Return the differences of integration of vectors one and two,
    normalized by the length of the vectors


    Input
    -----
    : one - np array
    : two - np array
    Returns
    -------
    : float - the Gini of the differences between one and two

    """

    return (np.sum(one) - np.sum(two)) / len(one)


def gini_diff(one, two):
    """Return the Gini of the difference between two vectors.

    Input
    -----
    : one - np array
    : two - np array
    Returns
    -------
    : float - the Gini of the differences between one and two

    """
    warnings.simplefilter("error", RuntimeWarning)
    try:
        dif = gini(one - two)
    except RuntimeWarning:
        raise ValueError("Something went very wrong")
    return dif


def generic_read_in(fhandle):
    """Read in the rate file from a file handle.

    Input
    -----
    : fhandle - the reactivity file handle,
              which must contain a bz2 or gz file.
              The file should contain each transcript
              data separated by tabs right after a line with the transcript
              name. The transcript name line contains the id of the transcript
              as the first word, separated by tabs, and contain "hit_rate".
    Ouput
    -----
    : data_struct - a dict with transcript name as key, and a numpy array as
                    values.
    """

    data_struct = {}
    for line in fhandle:
        if "hit_rate" in line:
            w = line.split("\t")[0]
            line = next(fhandle)
            line = line.replace("nan", "0.00").replace("inf", "0.00")
            data_struct[w] = np.array([float(x)
                                       for x in line.strip().split('\t')])
    return data_struct


def read_in_db(*, fname: str, react_col) -> Dict:
    """Read in the reactivities from the database

    Input
    -----
    : fname - the reactivity file name. This name should match the entry
              in the reactivity database.
    Ouput
    -----
    : data_struct - a dict with transcript name as key, and a numpy array as
                    values.
    """
    data_struct = {}
    query = {'fname': fname}
    for m in react_col.find(query):
        data_struct[m['transc_id']] = pickle.loads(m['reactivities'])

    return data_struct


def read_in(fname):
    """Read in the rate file.

    Input
    -----
    : fname - the reactivity file name. The file should contain each transcript
              data separated by tabs right after a line with the transcript
              name. The transcript name line contains the id of the transcript
              as the first word, separated by tabs, and contain "hit_rate".
    Ouput
    -----
    : data_struct - a dict with transcript name as key, and a numpy array as
                    values.
    """
    with open(fname) as fi:
        data_struct = generic_read_in(fi)

    return data_struct


def read_in_compressed_fo(fobject, extension='bz2'):
    """Read in the rate file.

    Input
    -----
    : fname - the reactivity file object, which must contain a bz2 or gz file.
              The file should contain each transcript
              data separated by tabs right after a line with the transcript
              name. The transcript name line contains the id of the transcript
              as the first word, separated by tabs, and contain "hit_rate".
    Ouput
    -----
    : data_struct - a dict with transcript name as key, and a numpy array as
                    values.
    """
    if extension == 'bz2':
        decompressor = bz2.open
    elif extension == 'gz':
        decompressor = gzip.open
    else:
        raise ValueError('Unknown extension {}'.format(extension))

    arguments = (fobject, 'rt')
    with decompressor(*arguments) as fi:
        data_struct = generic_read_in(fi)

    return data_struct


def read_seq_db(col_seq, select):
    """Query the sequence of a given transc_id in the db"""
    query = {'transc_id': select}
    return col_seq.find_one(query)['seq']


def read_selected_seqs_db(seqs_col, select):
    """Read the sequences, return on the ones in select.

    Input
    -----
    : seqs_col - collection holding the sequences
    : select - list of ids to keep from inp_fasta

    Ouput
    -----
    : records - dict of SeqIO records
    """
    records = {}
    query = {'transc_id': {"$in": select}}
    for m in seqs_col.find(query):
        records[m['transc_id']] = m['seq']
    return records


def read_selected_fasta(inp_fasta, select):
    """Read the sequences, return on the ones in select.

    Input
    -----
    : inp_fasta - fasta file, can be gz or bz2 compressed
    : select - list of ids to keep from inp_fasta

    Ouput
    -----
    : records - dict of SeqIO records
    """
    records = {}
    if inp_fasta.split(".")[-1] == "bz2":
        handle = bz2.BZ2File(inp_fasta, "r")
    elif inp_fasta.split(".")[-1] == "gz":
        handle = gzip.open(inp_fasta, "rt")
    else:
        handle = open(inp_fasta, "r")
    for seq_record in SeqIO.parse(handle, 'fasta'):
        if seq_record.id in select:
            records[seq_record.name] = str(seq_record.seq)
    return records


def search_gx(seq, regex):
    """Locate the regex in seq."""
    loc = []
    for m in regex.finditer(seq):
        loc.append(Range(m.start(), m.end()))
    return loc


def count_total_g4(inp_fasta: str, *, regex: Pattern):
    """Count the total number of G4s possible for all transcripts"""

    if inp_fasta.split(".")[-1] == "bz2":
        handle = bz2.BZ2File(inp_fasta, "r")
    elif inp_fasta.split(".")[-1] == "gz":
        handle = gzip.open(inp_fasta, "rt")
    else:
        handle = open(inp_fasta, "r")

    counts = 0
    for seq_record in SeqIO.parse(handle, 'fasta'):
        r = search_gx(str(seq_record.seq), regex)
        counts += len(r)
    return counts


def gx_insert_validated_regions(filt_tr, *, data: List[Dict],
                                slop_g4: int,
                                cntrl_col: List, db_name: str,
                                cntrl2check: str) -> Dict:
    """Add control regions to a dictionary

    Given a dictionary with keys trans_id and entries a list of ranges,
    which might be actually empty, fill in the regions from a positive
    control, i.e. where we know we could have rG4s.
    Only include them if there's reactivity data in them.

    Input
    -----
    :param slop_g4: extend the quadruplex by this amount both sides
    :param db_name:  name of the database holding all the collections
    :param filt_tr: the dictionary of keys transc_id and values list[regions]
    :param data: list of reactivity data sets
    :param cntrl_col: mongodb document with the control that we want to insert
    :param cntrl2check: name of the species in the control, to double check
                        when finding records in the collection

    Output
    ------
    : filt_tr : the dictionary with, possibly, the new entries
    """
    client = MongoClient(host=[gconf.MONGO_HOST_AND_PORT],
                         maxPoolSize=None)  # open a connection
    db = client[db_name]

    query_positive = {'control_type': {'$regex': cntrl2check}}
    if slop_g4 == 0:
        for col_n in cntrl_col:
            col = db[col_n]
            p_cursor = col.find(query_positive)
            for p in p_cursor:
                do_append = True
                # we only keep it if it is not empty in all data sets
                for data_set in data:
                    rg = Range(*p['range'])
                    try:
                        if sum(data_set[p['transc_id']][rg.start:rg.end]) == 0:
                            do_append = False
                    except KeyError as e:
                        # if transcript is not in reactivities, for whatever
                        # reason, I don't care, just skip
                        pass

                if do_append:
                    filt_tr.setdefault(p['transc_id'], []).append(
                        Range(*p['range']))
    else:
        for col_n in cntrl_col:
            col = db[col_n]
            p_cursor = col.find(query_positive)
            for p in p_cursor:
                do_append = True
                # we only keep it if it is not empty in all data sets
                for data_set in data:
                    try:
                        l_react = len(data_set[p['transc_id']])
                        new_s = p['range'][0] - slop_g4
                        new_e = p['range'][1] + slop_g4
                        new_s = 0 if new_s < 0 else new_s
                        new_e = l_react - 1 if new_e >= l_react else new_e
                        rg = Range(new_s, new_e)
                        if sum(data_set[p['transc_id']][rg.start:rg.end]) == 0:
                            do_append = False
                    except KeyError as e:
                        # if transcript is not in reactivities, for whatever
                        # reason, I don't care, just skip
                        pass

                if do_append:
                    filt_tr.setdefault(p['transc_id'], []).append(
                        Range(*p['range']))
    # Merge possible overlaps with pre-existing Range
    # We have to do it at the end because we don't know how the
    # queries are being appended to the dictionary
    for transc, g4s in filt_tr.items():
        filt_tr[transc] = merge_intervals(g4s, len_gap=1)

    return filt_tr


def gx_location_in_transcripts_populated(seqs, *, regex: Pattern,
                                         data: List[Dict]):
    """Return dict with tr names and gX location if there is reactivity data.

    Only returns those transcripts where we located GXs,
    and only those with rate data.

    Input
    -----
    :param seqs: dict of Seqrecords, where the keys are the transcript names
    :param regex: a compiled regular expression, e.g for G4s
    :param data: list of reactivity data sets

    Output
    ------
    filt_tr - a dictionary containing a list of intervals where the regex
                found hits. The intervals are a list, could have been a tuple.
    """
    filt_tr = {}
    for k, s in seqs.items():
        r = search_gx(s, regex)
        if len(r) > 0:
            # could be more than one
            for p in r:
                do_append = True
                # we only keep it if it is not empty in all data sets
                for data_set in data:
                    try:
                        if sum(data_set[k][p.start:p.end]) == 0:
                            do_append = False
                    except KeyError as e:
                        # if transcript is not in reactivities, for whatever
                        # reason, I don't care, just skip
                        pass
                if do_append:
                    filt_tr.setdefault(k, []).append(p)

    return filt_tr


def merge_intervals(intervals: List[Range], len_gap: int = 30) -> List[Range]:
    """Merge intervals if they are separated less than len_gap.

    Adapted from https://codereview.stackexchange.com/a/69249
    Input
    -----
    :param intervals: - an array of arrays, each sub array is type Range
    :param len_gap: - the minimum separations between intervals in the output

    Output:
    ------
    : merged - the merged array of arrays, each sub array type Range
    """
    sorted_by_lower_bound = sorted(intervals, key=lambda tup: tup.start)
    merged = []

    if len_gap < 0:
        raise ValueError("len_gap must be a positive integer")

    for higher in sorted_by_lower_bound:
        if not merged:
            merged.append(higher)
        else:
            lower = merged[-1]
            # test for intersection between lower and higher:
            # we know via sorting that lower.start <= higher.end
            if higher.start - lower.end <= len_gap:
                upper_bound = max(lower.end, higher.end)
                # replace by merged interval
                merged[-1] = Range(lower.start, upper_bound)
            else:
                merged.append(higher)

    return merged


def nong4_in_transcripts(seqs, tr_g4, tr_g2, data=None, len_frag=30):
    """Return dict with tr names and non-G4 location.

    Given a set of semi-open intervals, fills "the gaps".
    Only returns those transcripts where we have located G4s or G2s, and the
    regions that are not completely empty.

    Input
    -----
    : seqs - dict of seqrecord, keys are the transcript ids
    : tr_g4 - dict with G4 intervals, keys are the transcript ids
    : tr_g2 - dict with 'G2' intervals, keys are the transcript ids
    : data - list of dicts, each dict contains the reactivities
             of a given transcript
    : len_frag - the min length of an interval

    Output
    ------
    : filt_non_g4 - dict of non-G4 intervals, keys are transcript id
    """
    filt_non_g4 = {}
    for tr_name, g4s in tr_g4.items():

        # Iterating over a given transcript.
        # The first non-G4 interval will start presumably at 0,
        # except if the G4 actually starts at 0, which is not impossible
        # we avoid it by requiring that the interval is shorter than
        # the len_frag, which is the window size we will use
        # to "comb" the intervals
        b_frag = 0

        # MERGE G4 and G2 (if required) and then start filling the gaps
        if tr_name in tr_g2:
            merged_g4_g2 = merge_intervals(g4s + tr_g2[tr_name], len_frag)
        else:
            merged_g4_g2 = g4s

        for interval in merged_g4_g2:
            if interval.start - b_frag >= len_frag:
                chunk_b4 = Range(b_frag, interval.start)
                # only keep it if there is data in the fragment, at least
                # one data point, I don't want to check all intervals here
                do_append = True
                for data_set in data:
                    try:
                        if sum(data_set[tr_name][b_frag:interval.start]) == 0:
                            do_append = False
                    except KeyError as e:
                        # if transcript is not in reactivities, for whatever
                        #    reason, I don't care, just skip
                        pass
                if do_append:
                    filt_non_g4.setdefault(tr_name, []).append(chunk_b4)
            b_frag = interval.end + 1

        # the last bit, only if it has the required length and contains data
        try:
            if len(seqs[tr_name]) - b_frag >= len_frag:
                chunk_last = Range(b_frag, len(seqs[tr_name]))
                # only keep it if there is data in the fragment, at least
                # one data point, I don't want to check all intervals here
                do_append = True
                for data_set in data:
                    try:
                        if sum(data_set[tr_name][
                               b_frag:len(seqs[tr_name])]) == 0:
                            do_append = False
                    except KeyError as e:
                        # if transcript is not in reactivities, for whatever
                        #    reason, I don't care, just skip
                        pass
                if do_append:
                    filt_non_g4.setdefault(tr_name, []).append(chunk_last)
        except KeyError as e:
            pass

    return filt_non_g4


def compare_nong4(data1, data2, nong4_tr, len_frag=30, metric=rmsd):
    """Return a list of comparisons between data1 and data2 for nonG4s.

    The comparisons are carried out in windows of length len_frag
    using the function defined in metric, which should take two
    numpy arrays and return a float.

    Input
    -----
    : data1, data2 - dict of reactivities, keys are transcript id
    : nong4_tr - dict of non-G4 intervals, keys are the transcript id
    : len_frag - length of the window sliding across nonG4 intervals
    : metric - function taking two numpy arrays and returning float

    Output
    ------
    : metric nong4 - dict of lists containing the result of metric, keys are
                    the transcript ids
    """
    metric_nong4 = {}
    for tr_name in nong4_tr.keys():
        for chunck in nong4_tr[tr_name]:
            for b in range(chunck.start, chunck.end - len_frag):
                try:
                    val = metric(data1[tr_name][b:b + len_frag],
                                 data2[tr_name][b:b + len_frag])
                    if val is not None:
                        metric_nong4.setdefault(tr_name, []).append(val)
                except KeyError as e:
                    pass
        try:
            metric_nong4[tr_name] = np.array(metric_nong4[tr_name])
        except KeyError as e:
            pass
    return metric_nong4


def compare_g4(data1, data2, g4_tr, metric=rmsd):
    """Return a list of comparisons between data1 and data2 for G4.

    metric is a function to calculate dis/similarity between two vectors.

    Input
    -----
    : data1, data2 - dict of reactivities, keys are transcript id
    : g4_tr - dict of non-G4 intervals, keys are the transcript id
    : metric - function taking two numpy arrays and returning float

    Output
    ------
    : metric g4 - dict of lists containing the result of metric, keys are
                    the transcript ids
    """
    metric_g4 = {}
    for tr_name in g4_tr.keys():
        for chunck in g4_tr[tr_name]:
            try:
                val = metric(data1[tr_name][chunck.start:chunck.end],
                             data2[tr_name][chunck.start:chunck.end])
                if val is not None:
                    metric_g4.setdefault(tr_name, []).append(val)
            except KeyError as e:
                pass
        # to np.array
        try:
            metric_g4[tr_name] = np.array(metric_g4[tr_name])
        except KeyError as e:
            pass
    return metric_g4


def numerical_pvalue_gt(reference, sample):
    """Return a numerical p-value for reference being different to sample.

    The minimum p-value possible is 1.0 / (len(sample)). The pvalue is the
    fraction of values in sample that is *larger* than reference. This
    is the main difference between this implementation and numerical_pvalue()
    Larger or smaller depends on if the point lies to the left of mean(sample)
    or to its right.

    If the reference is not larger than the sample mean, we return 1

    Input
    -----
    : reference - float
    : sample  - list of floats
    Output
    ------
    : float
    """
    min_pval = 1.0 / len(sample)
    if reference > sample.mean():
        n = np.array([1 for val in sample if val > reference]).sum()
        pval = float(n) / float(len(sample))
    else:
        pval = 1

    return max(pval, min_pval)


def numerical_pvalue(reference, sample):
    """Return a numerical p-value for reference being different to sample.

    The minimum p-value possible is 1.0 / (len(sample)). The pvalue is the
    fraction of values in sample that is larger or smaller than reference.
    Larger or smaller depends on if the point lies to the left of mean(sample)
    or to its right.

    Input
    -----
    : reference - float
    : sample  - list of floats
    Output
    ------
    : float
    """
    min_pval = 1.0 / len(sample)
    if reference > sample.mean():
        n = np.array([1 for val in sample if val > reference]).sum()
    else:
        n = np.array([1 for val in sample if val < reference]).sum()
    pval = float(n) / float(len(sample))
    return max(pval, min_pval)


def filter_significance(w_g4=None, wo_g4=None, g4_tr=None,
                        pval_threshold=0.001):
    """Return the fragments with a metric significantly different to control.

   Different means showing larger differences.

    Input
    -----
    : w_g4 - dict containing the difference in metric in the test region
    : wo_g4 - dict containing the difference in metric in the control region

    Both w_G4 and wo_g4 the keys are the transcript name

    Returns
    -------
    : significant - dict with transcript name keys and intervals as values.
                    The intervals are for test regions (i.e. presumably G4s)
                    that are significantly different than the control regions.
    """
    significant = {}
    for tr_name in w_g4.keys():
        for index, g4_ref in enumerate(w_g4[tr_name]):
            # the controls wo_G4 could be empty for a given transcript
            # we need to check first
            if tr_name in wo_g4:
                num_pval = numerical_pvalue_gt(g4_ref, wo_g4[tr_name])
                if num_pval < pval_threshold:
                    b, e = g4_tr[tr_name][index]
                    signif = SignificantResult(start=b, end=e,
                                               pvalue=num_pval,
                                               metric=g4_ref,
                                               bg_metrics=wo_g4[
                                                   tr_name].tolist())

                    significant.setdefault(tr_name, []).append(signif)
    return significant


def compute_significant_g4_based(*, k_fn, li_fn, pds_fn, react_col, seqs_col,
                                 regex_g4, regex_g2, cntrl_col, db_name,
                                 len_frag=30
                                 ):
    """Do the work."""
    # Read in the reactivities
    k_only = read_in_db(fname=k_fn, react_col=react_col)
    k_pds = read_in_db(fname=pds_fn, react_col=react_col)
    li_only = read_in_db(fname=li_fn, react_col=react_col)

    # Read in the sequences, select only those for which we have reactivities
    select = list(k_only.keys())
    seqs = read_selected_seqs_db(seqs_col, select)
    g4_tr = gx_location_in_transcripts_populated(seqs,
                                                 regex=regex_g4,
                                                 data=[k_only,
                                                       k_pds,
                                                       li_only, ],
                                                 )

    g4_tr = gx_insert_validated_regions(g4_tr,
                                        data=[k_only, k_pds, li_only],
                                        cntrl_col=cntrl_col,
                                        cntrl2check='PDS',
                                        db_name=db_name,
                                        )

    g2_tr = gx_location_in_transcripts_populated(
        seqs, regex=regex_g2, data=[k_only, k_pds, li_only])

    nong4_tr = nong4_in_transcripts(seqs, g4_tr, g2_tr, data=[
        k_only, k_pds, li_only], len_frag=len_frag)

    pval_threshold = 0.005
    conditions = {'K': k_only, "Li": li_only, "PDS": k_pds}
    # lazy, will use itertools here...
    comparisons = {}
    for cond1, cond2 in itertools.combinations(conditions.keys(), 2):
        data_cond1 = conditions[cond1]
        data_cond2 = conditions[cond2]
        m_non_g4 = compare_nong4(data_cond1, data_cond2,
                                 nong4_tr, len_frag=len_frag, metric=gini_diff)
        m_g4 = compare_g4(data_cond1, data_cond2, g4_tr, metric=gini_diff)
        key = "-".join([cond1, cond2])
        comparisons[key] = {'nonG4': m_non_g4, "G4": m_g4}
    significant_changes = {}
    for compare in comparisons.keys():
        significant_changes[compare] = \
            filter_significance(w_g4=comparisons[compare]['G4'],
                                wo_g4=comparisons[compare]['nonG4'],
                                g4_tr=g4_tr,
                                pval_threshold=pval_threshold)

    return significant_changes
