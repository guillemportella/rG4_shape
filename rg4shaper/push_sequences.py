"""Add the sequences to the database

Each transcript sequence will be stored as a document.
This will make retrieving sequences much faster for react_viewer
"""

import bz2
import gzip
import logging
from collections import namedtuple
from contextlib import contextmanager

from Bio import SeqIO
from pymongo import MongoClient

DbCol = namedtuple('db_and_collection', ['db', 'collection'])

from rg4shaper.load_config import config

logger = logging.getLogger("rg4_logger")
MONGO_HOST_AND_PORT = config.rg4_react['MONGO_PORT_AND_HOST']


@contextmanager
def getcollection(*, db_n, col_n):
    client = MongoClient(host=[MONGO_HOST_AND_PORT])  # open a connection
    db = client[db_n]
    col = db[col_n]
    yield DbCol(db, col)


def push_sequences(*, fname: str, seq_col):
    """Push the sequences in fname to the database"""

    logger.info(f"Pushing sequences from {fname}")
    if fname.split(".")[-1] == "bz2":
        handle = bz2.BZ2File(fname, "r")
    elif fname.split(".")[-1] == "gz":
        handle = gzip.open(fname, "rt")
    else:
        handle = open(fname, "r")
    all_records = []
    for seq_record in SeqIO.parse(handle, 'fasta'):
        record = {
            "transc_id": seq_record.name,
            "seq": str(seq_record.seq)
        }
        all_records.append(record)

    seq_col.insert_many(all_records)

    logger.info("Indexing the collection based on transc_id")
    seq_col.create_index('transc_id')
    logger.info("Done indexing.")

    logger.info(f"Done pushing {fname}")


def sequences_db(args):
    """Push the sequences to the db"""
    db_name = args['db']
    col_name = args['cn']
    seq_name = args['seq'].name
    if not col_name:
        col_name = "sequences"

    b_clean_collection = args['clean_collection']
    logger.debug(f"Col name {col_name}, db {db_name}")

    with getcollection(db_n=db_name, col_n=col_name) as db_info:
        if b_clean_collection:
            db_info.db.drop_collection(db_info.collection)
            logger.info(f"Dropped collections {col_name}")
        else:
            push_sequences(fname=seq_name, seq_col=db_info.collection)

    return 1
