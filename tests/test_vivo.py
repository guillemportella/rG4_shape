import pytest

import rg4shaper.compare_conditions as cc
from rg4shaper.document_classes import Range


##############################################
# Merge intervals
##############################################

def test_merge_intervals_1():
    list_r = [Range(0, 10), Range(9, 10)]
    len_gap = 1
    assert (cc.merge_intervals(list_r, len_gap=len_gap)) \
           == [Range(0, 10)]


def test_merge_intervals_2():
    list_r = [Range(0, 10), Range(9, 10), Range(11, 20)]
    len_gap = 1
    assert (cc.merge_intervals(list_r, len_gap=len_gap)) \
           == [Range(0, 20)]


def test_merge_intervals_3():
    list_r = [Range(-20, 10), Range(9, 10), Range(11, 20)]
    len_gap = 1
    assert (cc.merge_intervals(list_r, len_gap=len_gap)) \
           == [Range(-20, 20)]


def test_merge_intervals_4():
    list_r = [Range(-20, 10), Range(9, 10), Range(11, 20), Range(32, 52)]
    len_gap = 10
    assert (cc.merge_intervals(list_r, len_gap=len_gap)) \
           == [Range(-20, 20), Range(32, 52)]


def test_merge_intervals_neg():
    list_r = [Range(0, 10), Range(9, 10)]
    with pytest.raises(ValueError):
        len_gap = -1
        cc.merge_intervals(list_r, len_gap=len_gap)
