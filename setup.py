from setuptools import setup, find_packages

setup(
    name="rG4_react",
    version="0.7.0",
    python_requires='>3.6',
    packages=find_packages(),
    include_package_data=True,
    scripts=['rg4_react', 'react_viewer'],

    # Project uses reStructuredText, so ensure that the docutils get
    # installed or upgraded on the target machine
    install_requires=['bokeh>0.12.14', 'pymongo', 'flask', 'pandas', 'numpy',
                      'biopython', 'pytest', 'tornado', 'attr', 'motor'],

    package_data={
        # If any package contains *.txt or *.rst files, include them:
        'rg4shaper': ['*.md', 'conf/*.conf'],
    },

    # metadata for upload to PyPI
    license="MIT",
    keywords="SHAPE reactivities",
    project_urls={
        "Source Code": "https://gitlab.com/guillemportella/rG4_shape.git",
    }

)
