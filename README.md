
## Code to analyse SHAPE reactivity data from rG4 experiments

The main tool for the analysis is `rg4_react.py`, which 
contails subtools to push reactivity data and controls to a MongoDB database, 
and perform the analysis and store them in that database.

There is a small interactive `bokeh` app embeded in a `flask` framework
to display the results of the analysis. Upon execution, `python react_viewer.py`,
the tool will open a server connection at http://127.0.0.1:8000/.
Actually, there will be two ports open, onet at localhost:5006 for the 
bokeh app itself, and flask's one at :8000. You should point to the `flask` one.



#### Using pipenv to manage dependencies

Using [pipenv](http://pipenv.readthedocs.io/en/latest/) to manage the depenencies.

For example, 

* Check the dependencies using pipenv graph. 
* Run a "virtualenv" aware shell with `pipenv shell`
* Execute within the "virtualenv" `pipenv run python xxx.py`, where `xxx.py` is
whatever you want to run.



