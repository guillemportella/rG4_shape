# TODO list


## Next 

- put template/static in program data, or else it won't be
 found by the installed version

- sort the displayed ranges and transcripts




## Soon

- Add min p-value as command line option?
- Scan results different definitions of G4
- Implement strategy B below
- Increase the rG4s to look for 
    1. change 7 to 12 in the loop
    2. ~~include the positive controls~~
    
- push the sequences to a db? might be faster to load


## Would be nice, but not a priority 

- Implement strategy B, this is:
    - Strategy B: "comb" all the regions with data and
    calculate distribution of (Gini?). Find significant 
    variations (e.g. outliers?, clustering?) and check
    if they are related to G4. 
    
- make "significant_rg4" read from database instead of 
tar file

- Fix visualization issues with sequence, maybe JavaScript??
- Add missing for which we actually had signal 
- Add consensus to visualization

### Done
- ~~Add control visualization~~
- ~~Push validations of controls to database~~


